# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
from tempfile import TemporaryDirectory
from unittest import TestCase
from unittest.mock import patch

from geode_gem.core.configuration import Configuration
from geode_gem.core.collection import (
    ConsoleCollection,
    GenericCollection,
    NativeCollection,
    ScummvmCollection,
    WineCollection,
)

from test.utils import FakeCore, FakeGame


class GenericCollectionTestCase(TestCase):
    BASIC_CONFIG = """
    [metadata]
    identifier = generic-collection
    launcher =

    [options]
    favorite = yes
    """

    def setUp(self):
        self.collection = GenericCollection("test")

    def test_constructor(self):
        self.assertEqual(self.collection.launcher, "")
        self.assertFalse(self.collection.favorite)

    def test_check_game_in_collection(self):
        self.assertFalse("game" in self.collection)
        self.collection["game"] = "fake-game"
        self.assertTrue("game" in self.collection)

    def test_remove_game_from_collection(self):
        with self.assertRaises(KeyError):
            del self.collection["game"]
        self.collection["game"] = "fake-game"
        del self.collection["game"]
        self.assertFalse("game" in self.collection)

    def test_get_game_from_collection(self):
        self.assertIsNone(self.collection["game"])
        self.collection["game"] = "fake-game"
        self.assertEqual(self.collection["game"], "fake-game")

    def test_iter_collection(self):
        for index in range(10):
            self.collection[f"game-{index}"] = f"fake-game-{index}"
        for index, game in enumerate(self.collection):
            self.assertEqual(game, f"fake-game-{index}")

    def test_get_collection_length(self):
        for index in range(10):
            self.collection[f"game-{index}"] = f"fake-game-{index}"
        self.assertEqual(len(self.collection), 10)
        del self.collection["game-4"]
        self.assertEqual(len(self.collection), 9)

    def test_add_game_to_collection(self):
        self.collection["id"] = "fake-game"
        self.assertEqual(self.collection["id"], "fake-game")

    def test_inspect_collection(self):
        self.collection["id"] = "fake-game"
        self.assertEqual(len(self.collection), 1)
        self.collection.inspect()
        self.assertEqual(len(self.collection), 0)

    def test_parse_configuration(self):
        config = Configuration()
        config.read_string(self.BASIC_CONFIG)

        data = GenericCollection.parse_configuration(config)
        self.assertEqual(data["launcher"], "")
        self.assertTrue(data["favorite"])


class ConsoleCollectionTestCase(TestCase):
    BASIC_CONFIG = """
    [metadata]
    identifier = console
    extensions = .nes .gb
    path = ~/roms

    [options]
    recursive = yes
    """

    def setUp(self):
        self.collection = ConsoleCollection("test")

    def test_add_game_from_outside_the_collection(self):
        with TemporaryDirectory() as directory:
            path = Path(directory, "roms")
            path.mkdir()

            self.collection.path = path
            with self.assertRaises(FileNotFoundError) as context:
                self.collection["test"] = FakeGame(
                    "test", Path(directory, "test.nes")
                )

            self.assertEqual(
                context.exception.args[0],
                "This game is not inside the collection",
            )

    def test_add_game_with_unauthorized_extension_in_the_collection(self):
        with TemporaryDirectory() as directory:
            path = Path(directory)
            self.collection.path = path
            self.collection.extensions = [".nes"]

            with self.assertRaises(KeyError) as context:
                self.collection["test"] = FakeGame(
                    "test", Path(directory, "test.gbc")
                )

            self.assertEqual(
                context.exception.args[0],
                "The extension of this game is not allowed by this collection",
            )

    def test_inspect_collection_without_path(self):
        with self.assertRaises(ValueError) as context:
            self.collection.inspect()

        self.assertEqual(
            context.exception.args[0], "This collection do not have a path"
        )

    def test_inspect_collection_with_unknown_path(self):
        with TemporaryDirectory() as directory:
            path = Path(directory, "unknown")
            self.collection.path = path

            with self.assertRaises(FileNotFoundError) as context:
                self.collection.inspect()

            self.assertEqual(
                context.exception.args[0],
                f"Cannot access to '{path}': No such file or directory",
            )

    def test_inspect_collection_with_file_as_path(self):
        with TemporaryDirectory() as directory:
            path = Path(directory, "test")
            path.touch()
            self.collection.path = path

            with self.assertRaises(NotADirectoryError) as context:
                self.collection.inspect()

            self.assertEqual(
                context.exception.args[0],
                f"Cannot list files with '{path}': Not a directory",
            )

    def test_inspect_collection(self):
        with TemporaryDirectory() as directory:
            path = Path(directory)
            for index in range(10):
                path.joinpath(f"game{index}.nes").touch()

            self.collection.path = path
            self.collection.extensions = [".nes"]
            self.collection.inspect()
            self.assertEqual(len(self.collection), 10)

    def test_inspect_collection_with_recursive_flag(self):
        with TemporaryDirectory() as directory:
            path = Path(directory)
            path.joinpath("sub0").mkdir()
            path.joinpath("sub1").mkdir()

            for index in range(10):
                path.joinpath(f"sub{index % 2}", f"game{index}.nes").touch()

            self.collection.path = path
            self.collection.extensions = [".nes"]
            self.collection.recursive = True
            self.collection.inspect()
            self.assertEqual(len(self.collection), 10)

    def test_is_extension_allowed(self):
        self.collection.extensions = [".nes", ".gb"]
        self.assertTrue(self.collection.is_extension_allowed(Path("test.nes")))
        self.assertFalse(self.collection.is_extension_allowed(Path("test.gbc")))

    def test_is_ignored(self):
        self.collection.ignored_patterns = ["so.+c"]
        self.assertFalse(self.collection.is_ignored(Path("mario.nes")))
        self.assertTrue(self.collection.is_ignored(Path("sonic.gg")))

    def test_parse_configuration(self):
        config = Configuration()
        config.read_string(self.BASIC_CONFIG)

        data = ConsoleCollection.parse_configuration(config)
        self.assertEqual(data["extensions"], [".nes", ".gb"])
        self.assertTrue(data["recursive"])


class NativeCollectionTestCase(TestCase):
    DESKTOP_GAME = """
    [Desktop Entry]
    Name=Test
    Icon=icon-of-test
    Exec=test
    Categories=Game;StrategyGame;
    """

    DESKTOP_NOT_GAME = """
    [Desktop Entry]
    Name=Test
    Icon=icon-of-test
    Exec=test
    Categories=Office;
    """

    def setUp(self):
        self.collection = NativeCollection("test")

    def test_inspect_collection_with_not_valid_path(self):
        with TemporaryDirectory() as directory:
            with patch("geode_gem.core.collection.xdg_data_dirs", [directory]):
                self.collection.inspect()
                self.assertEqual(len(self.collection), 0)

    def test_inspect_collection_with_valid_path(self):
        with TemporaryDirectory() as directory:
            with patch("geode_gem.core.collection.xdg_data_dirs", [directory]):
                path = Path(directory, "applications")
                path.mkdir()

                for index in range(5):
                    game = path.joinpath(f"game{index}.desktop")
                    game.write_text(self.DESKTOP_GAME)

                self.collection.inspect()
                self.assertEqual(len(self.collection), 5)
                self.assertEqual(self.collection["game1"].label, "Test")

    def test_inspect_collection_with_invalid_game(self):
        with TemporaryDirectory() as directory:
            with patch("geode_gem.core.collection.xdg_data_dirs", [directory]):
                path = Path(directory, "applications")
                path.mkdir()
                path.joinpath("game1.desktop").write_text(self.DESKTOP_GAME)
                path.joinpath("game2.desktop").write_text(self.DESKTOP_NOT_GAME)

                self.collection.inspect()
                self.assertEqual(len(self.collection), 1)


class ScummvmCollectionTestCase(TestCase):
    BASIC_CONFIG = """
    [metadata]
    identifier = generic-collection
    configuration = ~/.config/scummvm/scummvm.ini
    """

    SCUMMVM_INI = """
    [scummvm]
    confirm_exit=false

    [atlantis]
    gameid=atlantis
    description=Atlanta Jones and the Fate of India (CD/DOS/English)

    [lure]
    gameid=lure
    description=Fur of the Tigress (DOS/French)
    """

    def setUp(self):
        self.collection = ScummvmCollection("test")

    def test_inspect_collection(self):
        with TemporaryDirectory() as directory:
            path = Path(directory, "scummvm.ini")
            path.write_text(self.SCUMMVM_INI)

            self.collection.path = path
            self.collection.inspect()
            self.assertEqual(len(self.collection), 2)
            self.assertEqual(self.collection["atlantis"].identifier, "atlantis")
            self.assertEqual(
                self.collection["lure"].label, "Fur of the Tigress (DOS/French)"
            )

    def test_parse_configuration(self):
        config = Configuration()
        config.read_string(self.BASIC_CONFIG)

        data = ScummvmCollection.parse_configuration(config)
        self.assertEqual(data["path"].name, "scummvm.ini")


class WineCollectionTestCase(TestCase):
    WINE_GAME = """
    [metadata]
    identifier = test
    executable = test.exe
    launcher = wine-system
    prefix = ~/.wine

    [options]
    debug = yes
    """

    def setUp(self):
        self.collection = WineCollection("test")

    def test_inspect_collection(self):
        with TemporaryDirectory() as directory:
            data_path = Path(directory)

            collection_path = data_path.joinpath("wine", "test")
            collection_path.mkdir(parents=True)
            collection_path.joinpath("test.ini").write_text(self.WINE_GAME)

            core = FakeCore(data_path=data_path)
            self.collection.inspect(core)
            self.assertEqual(len(self.collection), 1)
            self.assertEqual(self.collection["test"].label, "test")
            self.assertEqual(self.collection["test"].launcher, "wine-system")
