# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from os import getpid
from pathlib import Path
from unittest import TestCase
from unittest.mock import patch
from tempfile import NamedTemporaryFile

from geode_gem.core.lock_file import (
    init_lock_file,
    read_lock_file,
    write_lock_file,
)


class LockFileTestCase(TestCase):
    @patch("geode_gem.core.lock_file.get_boot_datetime_as_timestamp")
    def test_init_lock_file_with_another_lock_active(self, mocked_function):
        mocked_function.return_value = 42.0

        with NamedTemporaryFile() as filepath:
            path = Path(filepath.name)
            path.write_text(f"{getpid()} 42.0")

            with self.assertRaises(FileExistsError) as context:
                init_lock_file(path)

        self.assertEqual(
            context.exception.args[0], "Already running with another PID"
        )

    @patch("geode_gem.core.lock_file.get_boot_datetime_as_timestamp")
    def test_init_lock_file_with_another_lock_unactive(self, mocked_function):
        mocked_function.return_value = 42.0

        with (
            NamedTemporaryFile() as filepath,
            patch("geode_gem.core.lock_file.write_lock_file") as write_patch,
        ):
            path = Path(filepath.name)
            path.write_text(f"{getpid()} 32.0")

            init_lock_file(path)

        write_patch.assert_called_once()

    @patch("geode_gem.core.lock_file.get_boot_datetime_as_timestamp")
    def test_init_lock_file_with_missing_uptime_file(self, mocked_function):
        mocked_function.return_value = None

        with self.assertRaises(FileNotFoundError) as context:
            init_lock_file(Path())

        self.assertEqual(
            context.exception.args[0], "Cannot access to /proc/uptime file"
        )

    @patch("geode_gem.core.lock_file.get_boot_datetime_as_timestamp")
    def test_init_lock_file_without_another_lock(self, mocked_function):
        mocked_function.return_value = 42.0

        with patch("geode_gem.core.lock_file.write_lock_file") as write_patch:
            init_lock_file(Path("unknown.file"))

        write_patch.assert_called_once()

    def test_read_lock_file_with_no_content(self):
        with NamedTemporaryFile() as filepath:
            path = Path(filepath.name)
            self.assertFalse(read_lock_file(path, "42.0"))

    def test_read_lock_file_with_equivalent_timestamps(self):
        with NamedTemporaryFile() as filepath:
            path = Path(filepath.name)
            path.write_text(f"{getpid()} 42.0")

            self.assertTrue(read_lock_file(path, "42.0"))

    def test_read_lock_file_with_missing_pid(self):
        with NamedTemporaryFile() as filepath:
            path = Path(filepath.name)
            path.write_text("-1 42.0")
            self.assertFalse(read_lock_file(path, "42.0"))

    def test_read_lock_file_with_unknown_previous_timestamp(self):
        with NamedTemporaryFile() as filepath:
            path = Path(filepath.name)
            path.write_text(f"{getpid()}")
            self.assertFalse(read_lock_file(path, "42.0"))

    def test_write_lock_file(self):
        with NamedTemporaryFile() as filepath:
            path = Path(filepath.name)
            write_lock_file(path, 42, 1337.0)
            self.assertEqual(path.read_text(), "42 1337.0")
