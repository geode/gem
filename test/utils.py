# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from dataclasses import dataclass
from pathlib import Path
from tempfile import NamedTemporaryFile

from geode_gem.core.configuration import Configuration


@dataclass
class FakeCore:
    data_path: Path

    def get_path_from_data(self, *args):
        return self.data_path.joinpath(*args)


@dataclass
class FakeGame:
    identifier: str
    path: Path | None = None


def generate_config(config_text: str) -> Configuration:
    """Generate a configuration object based on the specified text content

    Parameters
    ----------
    config_text : str
        The content of the configuration to parse

    Returns
    -------
    geode_gem.configuration.Configuration
        The generated configuration object
    """

    with NamedTemporaryFile() as file_buffer:
        path = Path(file_buffer.name)
        path.write_text(config_text)

        parser = Configuration(path)

    return parser
