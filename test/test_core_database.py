# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from sqlite3 import IntegrityError, OperationalError, sqlite_version
from tempfile import NamedTemporaryFile
from unittest import TestCase

from geode_gem.core.database import Database, parse_where
from geode_gem.core.game import ConsoleGame, GenericGame


class SimpleGame(GenericGame):
    def calc_checksum(self):
        self.checksum = self.identifier


class AdvancedGame(ConsoleGame):
    def calc_checksum(self):
        self.checksum = self.identifier


class ParseWhereTestCase(TestCase):
    def test_with_single_condition(self):
        self.assertEqual(parse_where(["hash = 1"]), ("hash = ?", ["1"]))
        self.assertEqual(parse_where(["hash like 1?"]), ("hash LIKE ?", ["1?"]))

    def test_with_multiple_conditions(self):
        self.assertEqual(
            parse_where(["hash = 42", "name = Test"]),
            ("hash = ? AND name = ?", ["42", "Test"]),
        )
        self.assertEqual(
            parse_where(["hash = 42", "name like Te?st"]),
            ("hash = ? AND name LIKE ?", ["42", "Te?st"]),
        )

    def test_with_invalid_condition(self):
        with self.assertRaises(ValueError) as context:
            parse_where(["name=1"])

        self.assertEqual(
            str(context.exception),
            "Spacing is mandatory to detect the symbols: 'name=1'",
        )


class DatabaseTestCase(TestCase):
    def setUp(self):
        # Generate a database with the default valid schema
        self.database = Database(":memory:")
        self.database.check_schemata()

    def test_alter_table_add_column(self):
        self.assertEqual(len(self.database.table_info("roms")), 5)
        self.database.alter_table_add_column("roms", "test", "TEXT DEFAULT ''")

        results = self.database.table_info("roms")
        self.assertEqual(len(results), 6)
        self.assertEqual(results[-1]["type"], "TEXT")

    def test_alter_table_drop_column(self):
        self.assertEqual(len(self.database.table_info("roms")), 5)
        self.database.alter_table_drop_column("roms", "emulator")
        self.assertEqual(len(self.database.table_info("roms")), 4)

    def test_alter_table_rename_column(self):
        self.database.alter_table_rename_column("games", "hash", "identifier")
        self.assertEqual(
            self.database.table_info("games")[0]["name"], "identifier"
        )

    def test_alter_table_rename_table(self):
        self.database.alter_table_rename_table("games", "juegos")
        self.assertEqual(len(self.database.table_info("games")), 0)
        self.assertEqual(len(self.database.table_info("juegos")), 11)

    def test_backup(self):
        self.database.insert("games", {"hash": 42, "label": "test"})

        with NamedTemporaryFile(suffix=".db") as context:
            self.database.backup(context.name)

            database = Database(context.name)
            results = database.select("games", "*", where=["hash = 42"])
            self.assertEqual(results[0]["label"], "test")

    def test_close_connection(self):
        self.assertIsNotNone(self.database.connection)
        self.database.close()
        self.assertIsNone(self.database.connection)

    def test_commit(self):
        self.assertFalse(self.database.connection.in_transaction)
        self.database.insert(
            "games", {"hash": 42, "label": "test"}, commit=False
        )
        self.assertTrue(self.database.connection.in_transaction)
        self.database.commit()
        self.assertFalse(self.database.connection.in_transaction)

        results = self.database.select("games", "*", where=["hash = 42"])
        self.assertEqual(results[0]["label"], "test")

    def test_commit_with_another_transaction(self):
        self.assertFalse(self.database.connection.in_transaction)
        self.database.insert(
            "games", {"hash": 42, "label": "test"}, commit=False
        )
        self.assertTrue(self.database.connection.in_transaction)

        self.database.insert("games", {"hash": 43, "label": "test2"})
        self.assertEqual(len(self.database.select("games", "*")), 2)

    def test_corrupt_database_with_unknown_column(self):
        self.database.alter_table_add_column("games", "pignouf", "TEXT")

        with self.assertLogs(level="INFO") as context:
            self.database.check_schemata()

        self.assertIn(
            "INFO:geode_gem.core.database:Database migration: remove the "
            "column 'pignouf' from the table games",
            context.output,
        )

        info = [
            element["name"] for element in self.database.table_info("games")
        ]
        self.assertNotIn("pignouf", info)

    def test_corrupt_database_with_missing_column(self):
        self.database.alter_table_drop_column("games", "label")

        with self.assertLogs(level="INFO") as context:
            self.database.check_schemata()

        self.assertIn(
            "INFO:geode_gem.core.database:Database migration: add the column "
            "'label' in the table games",
            context.output,
        )

        column = [
            element
            for element in self.database.table_info("games")
            if element["name"] == "label"
        ][0]
        self.assertEqual(column["dflt_value"], "''")
        self.assertEqual(column["name"], "label")
        self.assertEqual(column["notnull"], 1)

    def test_delete_all_content_from_table(self):
        for index in range(10):
            self.database.insert("games", {"hash": index})

        self.assertEqual(len(self.database.select("games", "*")), 10)

        self.database.delete("games")
        self.assertEqual(len(self.database.select("games", "*")), 0)

    def test_delete_specific_entries(self):
        for index in range(20):
            self.database.insert("games", {"hash": index})

        self.assertEqual(len(self.database.select("games", "*")), 20)

        self.database.delete("games", where=["hash like 1%"])
        self.assertEqual(len(self.database.select("games", "*")), 9)

    def test_drop_table_which_exists(self):
        self.assertEqual(len(self.database.table_info("games")), 11)
        self.database.drop_table("games")
        self.assertEqual(len(self.database.table_info("games")), 0)

    def test_drop_table_which_not_exists_and_with_if_exists(self):
        self.assertEqual(len(self.database.table_info("juegos")), 0)
        self.database.drop_table("juegos", if_exists=True)

    def test_drop_table_which_not_exists_and_without_if_exists(self):
        self.assertEqual(len(self.database.table_info("juegos")), 0)
        with self.assertRaises(OperationalError) as context:
            self.database.drop_table("juegos", if_exists=False)

        self.assertEqual(str(context.exception), "no such table: juegos")

    def test_get_data_from_existing_game(self):
        self.database.insert(
            "games", {"hash": "42", "label": "Test", "favorite": True}
        )

        game = SimpleGame(identifier=42)
        game.calc_checksum()

        data = self.database.get_data_from_game(game)
        self.assertEqual(
            data,
            {
                "hash": "42",
                "label": "Test",
                "icon": "",
                "favorite": 1,
                "finished": 0,
                "multiplayer": 0,
                "played": 0,
                "last_played": "",
                "last_played_time": "",
                "time_played": "",
                "environment": None,
            },
        )

    def test_get_data_from_unknown_game(self):
        game = SimpleGame(identifier=42)
        game.calc_checksum()

        data = self.database.get_data_from_game(game)
        self.assertEqual(len(data), 0)

    def test_get_data_with_additional_table_and_with_value(self):
        self.database.insert(
            "games", {"hash": "42", "label": "Test", "favorite": True}
        )
        self.database.insert("roms", {"hash": "42", "emulator": "nouttendo"})

        game = AdvancedGame(identifier=42)
        game.calc_checksum()

        data = self.database.get_data_from_game(game)
        self.assertEqual(
            data,
            {
                "hash": "42",
                "label": "Test",
                "icon": "",
                "favorite": 1,
                "finished": 0,
                "multiplayer": 0,
                "played": 0,
                "last_played": "",
                "last_played_time": "",
                "time_played": "",
                "environment": None,
                "arguments": "",
                "emulator": "nouttendo",
                "game_id": "",
                "id": 1,
            },
        )

    def test_get_data_with_additional_table_and_without_value(self):
        self.database.insert(
            "games", {"hash": "42", "label": "Test", "favorite": True}
        )

        game = AdvancedGame(identifier=42)
        game.calc_checksum()

        data = self.database.get_data_from_game(game)
        self.assertNotIn("emulator", data)

    def test_insert_data_without_null_value(self):
        self.database.insert("games", {"hash": "42", "label": "Test"})

    def test_insert_data_with_null_value(self):
        with self.assertRaises(IntegrityError):
            self.database.insert("roms", {"hash": "42", "emulator": None})

    def test_rollback(self):
        self.assertFalse(self.database.connection.in_transaction)
        self.database.insert(
            "games", {"hash": 42, "label": "test"}, commit=False
        )
        self.assertTrue(self.database.connection.in_transaction)
        self.database.rollback()
        self.assertFalse(self.database.connection.in_transaction)

        self.assertEqual(
            len(self.database.select("games", "*", where=["hash = 42"])), 0
        )

    def test_save_game_with_basic_game(self):
        self.database.insert("games", {"hash": 42, "label": "Test"})
        game = SimpleGame(42, label="Mari0")
        game.calc_checksum()

        self.database.save_game(game)
        results = self.database.select("games", "*", where=["hash = 42"])
        self.assertEqual(results[0]["label"], "Mari0")

    def test_save_game_with_multi_table_game(self):
        self.database.insert("games", {"hash": 42, "label": "Test"})
        game = AdvancedGame(42, label="Amazing Game", game_id="pignouf")
        game.calc_checksum()

        self.database.save_game(game)
        results = self.database.select("games", ["label"], where=["hash = 42"])
        self.assertEqual(results[0]["label"], "Amazing Game")
        results = self.database.select("roms", ["game_id"], where=["hash = 42"])
        self.assertEqual(results[0]["game_id"], "pignouf")

    def test_save_game_with_unkwnown_game(self):
        game = SimpleGame(42)
        game.calc_checksum()
        self.assertEqual(
            len(self.database.select("games", "*", where=["hash = 42"])), 0
        )

        self.database.save_game(game)
        self.assertEqual(
            len(self.database.select("games", "*", where=["hash = 42"])), 1
        )

    def test_select_with_no_data(self):
        self.assertEqual(len(self.database.select("games", "*")), 0)

    def test_select_with_some_data(self):
        self.database.insert("games", {"hash": 42, "label": "Test"})

        results = self.database.select("games", "*")
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0]["label"], "Test")
        self.assertEqual(results[0]["icon"], "")

    def test_select_with_specific_columns(self):
        self.database.insert(
            "games", {"hash": 42, "label": "Test", "favorite": True}
        )
        results = self.database.select("games", ["label"])
        self.assertEqual(results[0]["label"], "Test")

        with self.assertRaises(IndexError) as context:
            results[0]["favorite"]

        self.assertEqual(str(context.exception), "No item with that key")

    def test_select_with_where_condition(self):
        for index in range(20):
            self.database.insert(
                "games", {"hash": index, "label": f"Test{index}"}
            )

        results = self.database.select("games", "*", where=["hash = 42"])
        self.assertEqual(len(results), 0)

        results = self.database.select("games", "*", where=["hash = 1"])
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0]["label"], "Test1")

        results = self.database.select("games", "*", where=["hash like 1%"])
        self.assertEqual(len(results), 11)
        self.assertEqual(results[1]["label"], "Test10")

    def test_table_info_from_existing_table(self):
        self.assertEqual(len(self.database.table_info("games")), 11)

    def test_table_info_from_unknown_table(self):
        self.assertEqual(len(self.database.table_info("juegos")), 0)

    def test_update_all_content_from_table(self):
        for index in range(10):
            self.database.insert(
                "games", {"hash": index, "label": f"Test{index}"}
            )

        self.database.update("games", {"label": "Pignouf"})
        for element in self.database.select("games", "*"):
            self.assertEqual(element["label"], "Pignouf")

    def test_update_an_unknown_entry(self):
        self.database.update("games", {"label": "Test"}, ["hash = 42"])
        self.assertEqual(len(self.database.select("games", "*")), 0)

    def test_update_a_valid_entry_with_invalid_data(self):
        self.database.insert("games", {"hash": 42})

        with self.assertRaises(IntegrityError) as context:
            self.database.update("games", {"label": None}, ["hash = 42"])

        self.assertEqual(
            str(context.exception), "NOT NULL constraint failed: games.label"
        )

    def test_update_a_valid_entry_with_valid_data(self):
        self.database.insert("games", {"hash": 42, "label": "Test"})
        self.database.update(
            "games", {"label": "Amazing game", "favorite": True}, ["hash = 42"]
        )

        results = self.database.select("games", "*", where=["hash = 42"])
        self.assertEqual(results[0]["favorite"], 1)
        self.assertEqual(results[0]["label"], "Amazing game")

    def test_validate_database(self):
        for table, schema in self.database.schemata.items():
            database_info = self.database.table_info(table)

            for index in range(len(schema)):
                if schema[index].startswith("FOREIGN"):
                    continue

                info = database_info[index]
                self.assertTrue(
                    schema[index].startswith(f"{info['name']} {info['type']}")
                )

    def test_version(self):
        self.assertEqual(self.database.version, sqlite_version)
