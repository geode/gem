# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime, time, timedelta
from importlib import resources
from os import environ
from pathlib import Path
from tempfile import gettempdir, TemporaryDirectory
from unittest import TestCase

from geode_gem.core.collection import ConsoleCollection, GenericCollection
from geode_gem.core.game import GenericGame, ConsoleGame
from geode_gem.core.utils import (
    are_equivalent_timestamps,
    cast_str_to_type,
    check_path,
    generate_extension,
    generate_identifier,
    get_binary_path,
    get_boot_datetime_as_timestamp,
    get_creation_datetime,
    get_data,
    glob_escape,
    replace_special_keys_from_pattern,
    resource_from_path,
)


class UtilsTestCase(TestCase):
    def test_are_equivalent_timestamps(self):
        data = [
            (1587400601, 1587400601, 0, True),
            (1587400601, 1587400604, 0, False),
            (1587400601, 1587400604, 2, False),
            (1587400601, 1587400604, 3, True),
        ]

        for first, second, delta, result in data:
            self.assertEqual(
                are_equivalent_timestamps(first, second, delta=delta), result
            )

    def test_cast_str_to_bool(self):
        self.assertTrue(cast_str_to_type("yes", bool))
        self.assertFalse(cast_str_to_type("no", bool))

    def test_cast_str_to_float(self):
        self.assertEqual(cast_str_to_type("4.2", float), 4.2)

    def test_cast_str_to_integer(self):
        self.assertEqual(cast_str_to_type("25", int), 25)

    def test_cast_str_to_list(self):
        self.assertEqual(cast_str_to_type("hip 'hop'", list), ["hip", "'hop'"])
        self.assertEqual(
            cast_str_to_type("hip hop, eep", list, list_separator=","),
            ["hip hop", "eep"],
        )

    def test_cast_str_to_datetime(self):
        self.assertEqual(
            cast_str_to_type("2025-02-22", datetime), datetime(2025, 2, 22)
        )

    def test_cast_str_to_datetime_without_value(self):
        self.assertIsNone(cast_str_to_type("", datetime))

    def test_cast_str_to_time(self):
        self.assertEqual(cast_str_to_type("13:37:42", time), time(13, 37, 42))

    def test_cast_str_to_time_without_value(self):
        self.assertIsNone(cast_str_to_type("", time))

    def test_cast_str_to_timedelta(self):
        self.assertEqual(
            cast_str_to_type("86400.0", timedelta), timedelta(days=1)
        )

    def test_cast_str_to_timedelta_without_value(self):
        self.assertIsNone(cast_str_to_type("", timedelta))

    def test_cast_str_to_path(self):
        self.assertEqual(cast_str_to_type("~", Path), Path("~").expanduser())

    def test_cast_str_to_path_without_value(self):
        self.assertIsNone(cast_str_to_type("", Path))

    def test_cast_str_to_unknown(self):
        self.assertEqual(cast_str_to_type("test", None), "test")

    def test_check_path(self):
        self.assertIsNotNone(check_path("pyproject.toml"))
        self.assertIsNone(check_path("setup.py"))

        path = check_path("setup.py", fallback_path="pyproject.toml")
        self.assertIsNotNone(path)
        self.assertEqual(path.name, "pyproject.toml")

        self.assertIsNone(check_path("tox.ini", fallback_path="setup.py"))

    def test_generate_extension(self):
        self.assertEqual("[nN][eE][sS]", generate_extension("NES"))

        self.assertEqual(".[oO][gG][gG]", generate_extension(".ogg"))

        self.assertEqual(
            ".[tT][aA][rR].[xX][zZ]", generate_extension(".tAr.Xz")
        )

    def test_generate_identifier_from_valid_file(self):
        path = Path(gettempdir(), "Best Game in, the (World).gnu")
        path.touch()
        self.assertTrue(path.exists())

        self.assertEqual(
            f"best-game-in-the-world-gnu-{path.stat().st_ino}",
            generate_identifier(path),
        )
        path.unlink()

    def test_generate_identifier_from_unknown_file(self):
        path = Path("It's an unexistant_file!.obvious")
        self.assertFalse(path.exists())

        self.assertEqual(
            "it-s-an-unexistant-file-obvious", generate_identifier(path)
        )

    def test_generate_identifier_from_strings(self):
        strings = [
            ("Unexistant_File.ext", "unexistant-file-ext"),
            ("An  other File  .oops", "an-other-file-oops"),
        ]

        for string, result in strings:
            self.assertEqual(generate_identifier(string), result)

    def test_get_binary_path_with_valid_parameter(self):
        self.assertGreater(len(get_binary_path("python3")), 0)

    def test_get_binary_path_with_invalid_parameter(self):
        self.assertEqual(get_binary_path(None), [])
        self.assertEqual(get_binary_path(""), [])

    def test_get_binary_path_from_existing_file(self):
        with TemporaryDirectory(prefix="geode-gem-", suffix="-test") as path:
            filepath = Path(path).joinpath("binary_test")
            filepath.touch()
            self.assertEqual(get_binary_path(str(filepath)), ["binary_test"])

    def test_get_binary_path_from_unknown_command(self):
        self.assertEqual(len(get_binary_path("were-binary_of_doom")), 0)

    def test_get_binary_path_by_altering_environment(self):
        with TemporaryDirectory(prefix="geode-gem-", suffix="-test") as path:
            filepath = Path(path).joinpath("binary_test")
            filepath.touch()
            self.assertEqual(len(get_binary_path("binary_test")), 0)

            # Add a new entry into PATH environment to retrieve binary_test
            environ["PATH"] = f"{filepath.parent}:{environ['PATH']}"
            self.assertEqual(len(get_binary_path("binary_test")), 1)

            # Ensure to have an unreadable directory for current test
            filepath.parent.chmod(0o033)
            self.assertEqual(len(get_binary_path("binary_test")), 0)

    def test_get_boot_datetime_as_timestamp_without_proc(self):
        with self.assertRaises(FileNotFoundError) as context:
            get_boot_datetime_as_timestamp("unknown-path")

        self.assertEqual(
            context.exception.args[0], "Cannot found process file system"
        )

    def test_get_boot_datetime_as_timestamp_without_uptime(self):
        with TemporaryDirectory(prefix="geode-gem-", suffix="-test") as path:
            with self.assertRaises(FileNotFoundError) as context:
                get_boot_datetime_as_timestamp(path)

        self.assertEqual(
            context.exception.args[0],
            f"Cannot found {Path(path).joinpath('uptime')} on filesystem",
        )

    def test_get_boot_datetime_as_timestamp(self):
        with TemporaryDirectory(prefix="geode-gem-", suffix="-test") as path:
            uptime_path = Path(path, "uptime")
            uptime_path.touch()

            self.assertIsNone(get_boot_datetime_as_timestamp(path))

            with uptime_path.open("w") as pipe:
                pipe.write("1337.0 42.0")

            result = get_boot_datetime_as_timestamp(path)
            self.assertIsNotNone(result)

            self.assertAlmostEqual(
                result, datetime.now().timestamp() - 1337.0, delta=1
            )

    def test_get_creation_datetime_from_valid_file(self):
        creation_date = get_creation_datetime(
            get_data("test", "test_core_utils.py")
        )
        self.assertIs(type(creation_date), datetime)

    def test_get_creation_datetime_from_unknown_file(self):
        creation_date = get_creation_datetime("not_exists.file")
        self.assertIsNone(creation_date)

    def test_get_data_from_egg_file(self):
        path = get_data("__main__.py")
        self.assertTrue(path.exists())

    def test_get_data_from_outside_egg_file(self):
        path = get_data("test", "test_core_utils.py")
        self.assertTrue(path.exists())

        path = get_data("pyproject.toml")
        self.assertTrue(path.exists())

    def test_get_data_with_unknown_file(self):
        path = get_data("config", "not_exists.file")
        self.assertFalse(path.exists())

    def test_glob_escape(self):
        self.assertEqual(glob_escape("geode-gem"), "geode-gem")
        self.assertEqual(glob_escape("geode-gem *"), "geode-gem [*]")
        self.assertEqual(glob_escape("geode-gem ?_*"), "geode-gem [?]_[*]")

        self.assertEqual(glob_escape("geode-gem [!]"), "geode-gem [[]![]]")

        self.assertEqual(
            glob_escape("geode-gem [E][!]"), "geode-gem [[]E[]][[]![]]"
        )

        self.assertEqual(
            glob_escape("geode-gem [[E]]"), "geode-gem [[][[]E[]][]]"
        )

    def test_replace_special_keys_from_pattern(self):
        collection = GenericCollection("test-collection")

        game = GenericGame("test-game", path=Path("This-is-a-Test"))
        game.collection = collection

        self.assertEqual(
            replace_special_keys_from_pattern(game, "test-<filename>"),
            Path("test-This-is-a-Test").resolve(),
        )
        self.assertEqual(
            replace_special_keys_from_pattern(
                game, "<filepath>-<lower-filename>"
            ),
            Path("This-is-a-Test-this-is-a-test").resolve(),
        )

    def test_replace_special_keys_from_pattern_with_path_in_collection(self):
        collection = ConsoleCollection(
            "test-collection", path=Path("somewhere")
        )

        game = GenericGame("test-game", path=Path("This-is-a-Test"))
        game.collection = collection

        self.assertEqual(
            replace_special_keys_from_pattern(game, "<collection>-oufti"),
            Path("somewhere-oufti").resolve(),
        )

    def test_replace_special_keys_from_pattern_with_game_id(self):
        game = ConsoleGame("test-game", game_id="FOX")

        self.assertEqual(
            replace_special_keys_from_pattern(game, "KW<game-id>"),
            Path("KWFOX").resolve(),
        )

    def test_resource_from_path(self):
        path = resources.files("geode_gem").expanduser()
        self.assertEqual(
            resource_from_path("geode_gem", "data", "i18n"),
            path.joinpath("data", "i18n"),
        )
        self.assertEqual(
            resource_from_path("geode_gem", "data", "icons", "geode-gem.png"),
            path.joinpath("data", "icons", "geode-gem.png"),
        )

    def test_resource_from_unknown_file_in_egg(self):
        path = resources.files("geode_gem").expanduser()
        self.assertEqual(
            resource_from_path("geode_gem", "not_exists"),
            path.joinpath("not_exists"),
        )

    def test_resource_from_unknown_module(self):
        with self.assertRaises(ModuleNotFoundError):
            resources.files("geode_gemgem")
