# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path

from geode_gem.interface import utils


class TestGetPathFromString:
    def test_get_path_with_empty_string(self):
        home_path = Path("~").expanduser()

        path = utils.get_path_from_string("")
        assert path == home_path
        path = utils.get_path_from_string(" ")
        assert path == home_path

    def test_get_path_with_unknown_string(self):
        home_path = Path("~").expanduser()

        path = utils.get_path_from_string("pignouf")
        assert path == home_path

    def test_get_path_with_valid_string(self):
        path = utils.get_path_from_string("test")
        assert path.joinpath("test_interface_utils.py").exists()
