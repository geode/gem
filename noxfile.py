# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path

import nox


@nox.session(reuse_venv=True, tags=["documentation"])
def documentation(session):
    session.install(".[doc]")
    session.run(
        "sphinx-build", "-d", session.create_tmp(), "doc/source", "doc/build"
    )


@nox.session(reuse_venv=True, tags=["check", "style"])
def flake8(session):
    session.install("flake8")
    session.run("flake8")


@nox.session(reuse_venv=True, tags=["check", "style"])
def black(session):
    session.install("black")
    session.run(
        "black",
        "--check",
        "--diff",
        "--color",
        "--line-length=80",
        "geode_gem",
        "test",
    )


@nox.session(reuse_venv=True, tags=["style"])
def black_apply(session):
    session.install("black")
    session.run("black", "--line-length=80", "geode_gem", "test")


@nox.session(reuse_venv=True)
@nox.parametrize("python", ["3.11", "3.12", "3.13"])
def tests(session):
    session.install(".", "coverage", "pytest")
    try:
        session.run("coverage", "run", "-m", "pytest", *session.posargs)
    finally:
        session.run("coverage", "html", "-d", Path("htmlcov", session.python))
