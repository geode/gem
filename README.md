Geode-Gem
=========

Geode-Gem (former GEM) is a GTK+ Graphical User Interface (GUI) for GNU/Linux
which allows you to easily manage your emulators and games collection. This
software aims to stay the simplest.

![Geode-Gem main interface](preview.jpg)

Licenses
--------

The Geode-Gem application is a free software redistribute under the term of the
[GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.html) version
3.0 or later.

The Geode-Gem logo was made with the [GIMP](https://www.gimp.org/) software
and redistribute under the term of the [Free Art License](http://artlibre.org/licence/lal/en/).

The consoles icons used by Geode-Gem come from the [Evan-Amos](https://commons.wikimedia.org/wiki/User:Evan-Amos)
gallery and are [Public Domain](https://en.wikipedia.org/wiki/Public_domain).

Emulators
---------

Default configuration files allow you to use the following emulators out of the
box:

* [Mednafen](https://mednafen.github.io/)
* [Stella](https://stella-emu.github.io/) (Atari 2600)
* [Hatari](https://hatari.tuxfamily.org/) (Atari ST)
* [Fceux](https://fceux.com/web/home.html) (Nintendo NES)
* [Nestopia](http://0ldsk00l.ca/nestopia/) (Nintendo NES)
* [Mupen64plus](https://mupen64plus.org/) (Nintendo 64)
* [Desmume](https://desmume.org/) (Nintendo DS)
* [Dolphin](https://dolphin-emu.org/) (Nintendo GameCube et Nintendo Wii)
* [Gens](http://www.gens.me/index.shtml) (Sega Genesis)
* [DosBOX](https://dosbox-staging.github.io/)

The emulator licenses information have been stored into the [EMULATORS.md](EMULATORS.md)
file.

Authors
-------

* [PacMiam](https://pacmiam.kawateam.dev/) - Main developper

Packages
--------

This application is available on the following websites:

* [Pypi](https://pypi.org/project/Geode-Gem/)
* [Archlinux User Repository](https://aur.archlinux.org/packages/geode-gem)

Dependencies
------------

### Python

* python3 >= 3.11
* python3-gobject
* python3-setuptools
* python3-xdg

### System

* file
* gnome-icon-theme
* gnome-icon-theme-symbolic
* gobject-introspection
* gtk+3
* gtksourceview
* libgirepository
* libgirepository-devel
* librsvg
* xdg-utils

Retrieve source code
--------------------

To retrieve source code, you just need to use [git](https://git-scm.com/) and
run the following command:
```
git clone https://laboratory.kawateam.dev/Geode/gem.git geode-gem
```

You can also retrieve an archive from the [Geode-Gem download repository](https://archives.kawateam.dev/geode-gem/).

### Manual installation

Create a Python virtual environment in the directory of your choice and
activate it:
```
python3 -m venv /tmp/geode-gem
. /tmp/geode-gem/bin/activate
```

Now, install this application with `pip`:
```
pip install /path/to/the/clone/geode-gem
```

Geode-Gem can now be started with the `geode-gem` command.
