# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser
from errno import EACCES, EBUSY, EIO, EINTR, EINVAL, ELIBACC
from os import getenv
from sqlite3 import OperationalError
import logging

from geode_gem.application import Application
from geode_gem.core import Core
from geode_gem.core.metadata import Metadata


def main():
    """Main launcher

    Returns
    -------
    int
        The errno system symbol based on application status
    """

    parser = ArgumentParser(
        epilog=f"Copyleft {Metadata.YEARS} {Metadata.AUTHOR}",
        description=f"{Metadata.NAME} {Metadata.VERSION} ({Metadata.CODENAME})",
        conflict_handler="resolve",
    )

    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version=(
            f"{Metadata.NAME} {Metadata.VERSION} "
            f"({Metadata.CODENAME}) - {Metadata.LICENSE}"
        ),
        help="show the current version",
    )
    parser.add_argument(
        "-d", "--debug", action="store_true", help="set the log level as DEBUG"
    )

    parser_core = parser.add_argument_group("core arguments")
    parser_core.add_argument(
        "--profile",
        action="store",
        metavar="NAME",
        default="default",
        help=f"the profile name to use with {Metadata.NAME} (default: default)",
    )

    arguments = parser.parse_args()

    if arguments.debug:
        log_msg = "%(asctime)s %(levelname)9s | %(threadName)s | %(message)s"
    else:
        log_msg = "%(asctime)s %(levelname)9s | %(message)s"

    # Logging instance
    logging.basicConfig(
        format=log_msg,
        datefmt="%X",
        level=logging.DEBUG if arguments.debug else logging.INFO,
    )

    logger = logging.getLogger(Metadata.Python.APPLICATION)

    # Generate core instance
    logger.info("Initialization of the Geode-Gem core…")
    core = Core()

    try:
        core.read_profile(arguments.profile)
    except OperationalError as error:
        logger.error(f"Cannot read profile: {error}")
        return EBUSY

    logger.info(
        "The core was successfully initialized with the profile "
        f"'{arguments.profile}'"
    )

    try:
        if len(getenv("DISPLAY", "")) == 0:
            logger.error(f"Cannot launch {Metadata.NAME} without display")
            return EIO

        # Generate and start main application
        application = Application(core, logger)
        application.run()

    except KeyboardInterrupt:
        logger.warning("Terminate by keyboard interrupt")
        return EINTR

    except FileExistsError as error:
        # The lock file exists and still alive
        logger.error(error)
        return EBUSY

    except FileNotFoundError as error:
        # Cannot found the /proc/uptime file on filesystem
        logger.error(error)
        return EACCES

    except ImportError:
        logger.exception("An error occur during modules importation")
        return ELIBACC

    except Exception:
        logger.exception("An error occur during execution")
        return EINVAL

    return 0


if __name__ == "__main__":
    main()
