# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from gettext import gettext as _

from gi.repository import Gio, GLib, Gtk

from geode_gem.interface.about import GeodeGemAboutDialog
from geode_gem.interface.utils import generate_radio_menuitem


class HeaderBar(Gtk.HeaderBar):
    def __init__(self, interface, *args, **kwargs):
        """Constructor

        Parameters
        ----------
        interface : geode_gem.interface.GeodeGemInterface
            The instance of the main interface
        """

        super().__init__(*args, **kwargs)

        self.application = interface.application
        self.config = interface.config
        self.interface = interface

        self.menu_main_quit = Gio.Menu()
        self.menu_main_quit.append(_("Quit"), "win.quit")

        self.menu_main = Gio.Menu()
        self.menu_main.append(_("About..."), "win.about")
        self.menu_main.append_section(None, self.menu_main_quit)

        self.menu_view_collections = Gio.Menu()
        self.menu_view_collections.append(
            _("Hide Empty Collections"), "win.hide_collections"
        )

        self.menu_view_games = Gio.Menu()

        self.menu_view_sidebar = Gio.Menu()
        self.menu_item_sidebar_horizontal = generate_radio_menuitem(
            _("Horizontal"),
            "win.sidebar_orientation",
            GLib.Variant.new_uint16(Gtk.Orientation.HORIZONTAL),
        )
        self.menu_item_sidebar_vertical = generate_radio_menuitem(
            _("Vertical"),
            "win.sidebar_orientation",
            GLib.Variant.new_uint16(Gtk.Orientation.VERTICAL),
        )
        self.menu_view_sidebar_orientation = Gio.Menu()
        self.menu_view_sidebar_orientation.append_item(
            self.menu_item_sidebar_horizontal
        )
        self.menu_view_sidebar_orientation.append_item(
            self.menu_item_sidebar_vertical
        )
        self.menu_view_sidebar.append(_("Show sidebar"), "win.sidebar")
        self.menu_view_sidebar.append_section(
            None, self.menu_view_sidebar_orientation
        )
        self.menu_item_sidebar_no_icon = generate_radio_menuitem(
            _("Hide the image"),
            "win.sidebar_icon",
            GLib.Variant.new_uint16(Gtk.ImageType.EMPTY),
        )
        self.menu_item_sidebar_only_icon = generate_radio_menuitem(
            _("Show the game icon"),
            "win.sidebar_icon",
            GLib.Variant.new_uint16(Gtk.ImageType.ICON_NAME),
        )
        self.menu_item_sidebar_prefer_screenshot = generate_radio_menuitem(
            _("Show the game screenshot"),
            "win.sidebar_icon",
            GLib.Variant.new_uint16(Gtk.ImageType.PIXBUF),
        )
        self.menu_view_sidebar_icon = Gio.Menu()
        self.menu_view_sidebar_icon.append_item(self.menu_item_sidebar_no_icon)
        self.menu_view_sidebar_icon.append_item(
            self.menu_item_sidebar_only_icon
        )
        self.menu_view_sidebar_icon.append_item(
            self.menu_item_sidebar_prefer_screenshot
        )
        self.menu_view_sidebar.append_section(None, self.menu_view_sidebar_icon)

        self.menu_view_options = Gio.Menu()
        self.menu_view_options.append(_("Dark theme variant"), "win.dark_theme")

        self.menu_view = Gio.Menu()
        self.menu_view.append_submenu(
            _("Collections"), self.menu_view_collections
        )
        self.menu_view.append_submenu(_("Games"), self.menu_view_games)
        self.menu_view.append_submenu(
            _("Informative sidebar"), self.menu_view_sidebar
        )
        self.menu_view.append_section(None, self.menu_view_options)

        self.button_menu = Gtk.MenuButton(
            image=Gtk.Image(
                icon_name="open-menu-symbolic",
                icon_size=Gtk.IconSize.BUTTON,
                margin_end=2,
            ),
            menu_model=self.menu_main,
            tooltip_text=_("Main menu"),
            use_popover=True,
        )

        self.button_view = Gtk.MenuButton(
            image=Gtk.Image(
                icon_name="video-display-symbolic",
                icon_size=Gtk.IconSize.BUTTON,
                margin_end=2,
            ),
            menu_model=self.menu_view,
            tooltip_text=_("View"),
            use_popover=True,
        )

        self.grid_menu = Gtk.Box()
        self.grid_menu.add(self.button_menu)
        self.grid_menu.add(self.button_view)

        self.pack_start(self.grid_menu)

        Gtk.StyleContext.add_class(self.grid_menu.get_style_context(), "linked")

    def connect_widgets(self):
        """Connect signals to the internal widgets"""

        action = self.interface.register_action("hide_collections", state=True)
        action.connect("change-state", self.on_activate_hide_collections)

        action = self.interface.register_action("about")
        action.connect("activate", self.on_show_about_dialog)

        action = self.interface.register_action("quit")
        action.connect("activate", self.application.on_quit)

        action = self.interface.register_action(
            "dark_theme",
            state=self.config.getboolean(
                "theme",
                "is-dark",
                fallback=self.interface.settings.get_property(
                    "gtk-application-prefer-dark-theme"
                ),
            ),
        )
        action.connect("change-state", self.on_change_dark_theme_variant)

        action = self.interface.register_action(
            "sidebar",
            state=self.config.getboolean("sidebar", "visible", fallback=True),
        )
        action.connect("change-state", self.on_change_sidebar_visibility)

        variant = self.menu_item_sidebar_horizontal.get_attribute_value(
            Gio.MENU_ATTRIBUTE_TARGET, None
        )
        orientation = self.config.get(
            "sidebar", "orientation", fallback="horizontal"
        )
        action = self.interface.register_action(
            "sidebar_orientation",
            parameter_type=variant.get_type(),
            state=(
                Gtk.Orientation.HORIZONTAL
                if orientation == "horizontal"
                else Gtk.Orientation.VERTICAL
            ),
        )
        action.connect("change-state", self.on_change_sidebar_orientation)

        match self.config.get("sidebar", "icon-mode", fallback="both"):
            case "none":
                state = Gtk.ImageType.EMPTY
            case "icon-only":
                state = Gtk.ImageType.ICON_NAME
            case _:
                state = Gtk.ImageType.PIXBUF

        self.interface.register_action(
            "sidebar_icon",
            parameter_type=self.menu_item_sidebar_no_icon.get_attribute_value(
                Gio.MENU_ATTRIBUTE_TARGET, None
            ).get_type(),
            state=state,
        ).connect("change-state", self.on_change_sidebar_icon_mode)

    def on_activate_hide_collections(self, action, variant):
        """Update the list of collections to show or hide empty one

        Parameters
        ----------
        action : gi.repository.Gio.SimpleAction
            The action which received the signal
        variant : gi.repository.GLib.Variant
            The new value for the state
        """

        action.set_state(variant)

        self.interface.collections.invalidate()

    def on_change_dark_theme_variant(self, action, variant):
        """Update the dark theme based on the menu selection

        Parameters
        ----------
        action : gi.repository.Gio.SimpleAction
            The action which received the signal
        variant : gi.repository.GLib.Variant
            The new value for the state
        """

        action.set_state(variant)
        self.interface.toggle_dark_theme(variant.get_boolean())

    def on_change_sidebar_icon_mode(self, action, variant):
        """Update the game icon mode of the sidebar

        Parameters
        ----------
        action : gi.repository.Gio.SimpleAction
            The action which received the signal
        variant : gi.repository.GLib.Variant
            The new value for the state
        """

        icon_mode = variant.get_uint16()

        # Only update if the user do not select the same option again
        if not action.get_state().get_uint16() == icon_mode:
            action.set_state(variant)

            self.interface.set_sidebar_icon_mode(icon_mode)

            match icon_mode:
                case Gtk.ImageType.EMPTY:
                    mode = "none"
                case Gtk.ImageType.ICON_NAME:
                    mode = "icon-only"
                case Gtk.ImageType.PIXBUF:
                    mode = "both"

            self.config.update_option("sidebar", "icon-mode", mode)

    def on_change_sidebar_orientation(self, action, variant):
        """Update the sidebar orientation based on the menu selection

        Parameters
        ----------
        action : gi.repository.Gio.SimpleAction
            The action which received the signal
        variant : gi.repository.GLib.Variant
            The new value for the state
        """

        orientation = variant.get_uint16()

        # Only update if the user do not select the same option again
        if not action.get_state().get_uint16() == orientation:
            action.set_state(variant)

            self.interface.set_sidebar_orientation(orientation)

            self.config.update_option(
                "sidebar",
                "orientation",
                (
                    "horizontal"
                    if orientation == Gtk.Orientation.HORIZONTAL
                    else "vertical"
                ),
            )

    def on_change_sidebar_visibility(self, action, variant):
        """Update the sidebar visibility based on the menu selection

        Parameters
        ----------
        action : gi.repository.Gio.SimpleAction
            The action which received the signal
        variant : gi.repository.GLib.Variant
            The new value for the state
        """

        action.set_state(variant)

        visibility = variant.get_boolean()

        self.interface.set_sidebar_visibility(visibility)
        self.config.update_option("sidebar", "visible", visibility)

    def on_show_about_dialog(self, *args):
        """Presents the about dialog to the user"""

        about = GeodeGemAboutDialog()
        about.run()
        about.close()
