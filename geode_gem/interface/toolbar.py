# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from gettext import gettext as _

from gi.repository import Gio, GLib, Gtk

from geode_gem.configuration import GamesColumns
from geode_gem.interface.utils import (
    add_style_class_to_widget,
    block_interface_signals,
    generate_radio_menuitem,
)


class ToolbarCollections(Gtk.Box):
    def __init__(self, interface, *args, **kwargs):
        """Constructor

        Parameters
        ----------
        interface : geode_gem.interface.GeodeGemInterface
            The instance of the main interface
        """

        super().__init__(*args, **kwargs)

        self.interface = interface

        self.search_entry = Gtk.SearchEntry(placeholder_text=_("Filter..."))
        self.pack_start(self.search_entry, True, True, 0)

    def connect_widgets(self):
        """Connect signals to the internal widgets"""

        self.search_entry.connect(
            "changed", self.interface.collections.invalidate
        )


class ToolbarGames(Gtk.Box):
    FILTERS_LIST = (
        "filter_favorite",
        "filter_unfavorite",
        "filter_multiplayer",
        "filter_singleplayer",
        "filter_finished",
        "filter_unfinished",
    )

    def __init__(self, interface, *args, **kwargs):
        """Constructor

        Parameters
        ----------
        interface : geode_gem.interface.GeodeGemInterface
            The instance of the main interface
        """

        super().__init__(*args, **kwargs)

        self.application = interface.application
        self.config = interface.config
        self.interface = interface

        self.menu_filters = Gio.Menu()
        self.menu_filters_favorite = Gio.Menu()
        self.menu_filters_multiplayer = Gio.Menu()
        self.menu_filters_finished = Gio.Menu()
        self.menu_filters_reset = Gio.Menu()

        self.menu_filters.append_section(None, self.menu_filters_favorite)
        self.menu_filters.append_section(None, self.menu_filters_multiplayer)
        self.menu_filters.append_section(None, self.menu_filters_finished)
        self.menu_filters.append_section(None, self.menu_filters_reset)

        self.menu_filters_favorite.append(_("Favorite"), "win.filter_favorite")
        self.menu_filters_favorite.append(
            _("Unfavorite"), "win.filter_unfavorite"
        )
        self.menu_filters_multiplayer.append(
            _("Multiplayer"), "win.filter_multiplayer"
        )
        self.menu_filters_multiplayer.append(
            _("Singleplayer"), "win.filter_singleplayer"
        )
        self.menu_filters_finished.append(_("Finished"), "win.filter_finished")
        self.menu_filters_finished.append(
            _("To Finish"), "win.filter_unfinished"
        )
        self.menu_filters_reset.append(_("Reset Filters"), "win.filter_reset")

        self.menu_sort = Gio.Menu()
        self.menu_sort_columns = Gio.Menu()
        self.menu_sort_order = Gio.Menu()

        self.menu_sort_favorite = generate_radio_menuitem(
            _("Favorite"),
            "win.sort_column",
            GLib.Variant.new_uint16(GamesColumns.FAVORITE),
        )
        self.menu_sort_label = generate_radio_menuitem(
            _("Label"),
            "win.sort_column",
            GLib.Variant.new_uint16(GamesColumns.LABEL),
        )
        self.menu_sort_finished = generate_radio_menuitem(
            _("Finished"),
            "win.sort_column",
            GLib.Variant.new_uint16(GamesColumns.FINISHED),
        )
        self.menu_sort_multiplayer = generate_radio_menuitem(
            _("Multiplayer"),
            "win.sort_column",
            GLib.Variant.new_uint16(GamesColumns.MULTIPLAYER),
        )
        self.menu_sort_columns.append_item(self.menu_sort_favorite)
        self.menu_sort_columns.append_item(self.menu_sort_label)
        self.menu_sort_columns.append_item(self.menu_sort_finished)
        self.menu_sort_columns.append_item(self.menu_sort_multiplayer)

        self.menu_sort_ascending = generate_radio_menuitem(
            _("Ascending"),
            "win.sort_order",
            GLib.Variant.new_uint16(Gtk.SortType.ASCENDING),
        )
        self.menu_sort_descending = generate_radio_menuitem(
            _("Descending"),
            "win.sort_order",
            GLib.Variant.new_uint16(Gtk.SortType.DESCENDING),
        )
        self.menu_sort_order.append_item(self.menu_sort_ascending)
        self.menu_sort_order.append_item(self.menu_sort_descending)

        self.menu_sort.append_section(None, self.menu_sort_columns)
        self.menu_sort.append_section(None, self.menu_sort_order)

        self.image_play = Gtk.Image(
            icon_name="media-playback-start-symbolic",
            icon_size=Gtk.IconSize.BUTTON,
            margin_end=2,
        )
        self.button_play = Gtk.Button(label=_("Play"), image=self.image_play)
        add_style_class_to_widget(
            Gtk.STYLE_CLASS_SUGGESTED_ACTION, self.button_play
        )

        self.button_properties = Gtk.Button(
            image=Gtk.Image(
                icon_name="document-edit-symbolic",
                icon_size=Gtk.IconSize.BUTTON,
            )
        )
        self.button_screenshots = Gtk.Button(
            image=Gtk.Image(
                icon_name="camera-photo-symbolic", icon_size=Gtk.IconSize.BUTTON
            )
        )
        self.button_log = Gtk.Button(
            image=Gtk.Image(
                icon_name="utilities-terminal-symbolic",
                icon_size=Gtk.IconSize.BUTTON,
            )
        )
        self.button_notes = Gtk.Button(
            image=Gtk.Image(
                icon_name="accessories-text-editor-symbolic",
                icon_size=Gtk.IconSize.BUTTON,
            )
        )

        self.searchentry = Gtk.SearchEntry(
            placeholder_text=_("Filter..."),
            sensitive=False,
        )
        self.button_filters = Gtk.MenuButton(
            use_popover=True,
            menu_model=self.menu_filters,
            image=Gtk.Image(
                icon_name="view-more-symbolic", icon_size=Gtk.IconSize.BUTTON
            ),
            sensitive=False,
        )

        self.image_sort = Gtk.Image(
            icon_name=(
                "view-sort-ascending-symbolic"
                if self.config.games_sorted_order == Gtk.SortType.ASCENDING
                else "view-sort-descending-symbolic"
            ),
            icon_size=Gtk.IconSize.BUTTON,
        )
        self.button_sort = Gtk.MenuButton(
            use_popover=True,
            menu_model=self.menu_sort,
            image=self.image_sort,
            sensitive=False,
        )

        self.add(self.button_play)
        self.add(self.button_properties)

        self.grid_tools = Gtk.Box()
        self.grid_tools.add(self.button_screenshots)
        self.grid_tools.add(self.button_log)
        self.grid_tools.add(self.button_notes)
        self.add(self.grid_tools)

        self.grid_filters = Gtk.Box()
        self.grid_filters.add(self.searchentry)
        self.grid_filters.add(self.button_filters)

        self.pack_end(self.button_sort, False, False, 0)
        self.pack_end(self.grid_filters, False, False, 0)

        for box in (self.grid_tools, self.grid_filters):
            add_style_class_to_widget(Gtk.STYLE_CLASS_LINKED, box)

        self.reset()

    def connect_widgets(self):
        """Connect signals to the internal widgets"""

        variant = self.menu_sort_favorite.get_attribute_value(
            Gio.MENU_ATTRIBUTE_TARGET, None
        )
        action = self.interface.register_action(
            "sort_column",
            parameter_type=variant.get_type(),
            state=self.config.games_sorted_column,
        )
        action.connect("change-state", self.on_change_sort_column)

        variant = self.menu_sort_ascending.get_attribute_value(
            Gio.MENU_ATTRIBUTE_TARGET, None
        )
        action = self.interface.register_action(
            "sort_order",
            parameter_type=variant.get_type(),
            state=self.config.games_sorted_order,
        )
        action.connect("change-state", self.on_change_sort_order)

        for flag in self.FILTERS_LIST:
            action = self.interface.register_action(flag, state=True)
            action.connect("change-state", self.on_update_filter)

        self.searchentry.connect(
            "search-changed", self.interface.games.invalidate
        )

        action = self.interface.register_action("filter_reset")
        action.connect("activate", self.on_reset_filters)

        self.button_properties.connect(
            "clicked", self.interface.on_edit_game_properties
        )
        self.button_screenshots.connect(
            "clicked", self.interface.on_open_screenshots_viewer
        )
        self.button_log.connect("clicked", self.interface.on_open_log_viewer)
        self.button_notes.connect(
            "clicked", self.interface.on_open_notes_viewer
        )

        self.button_play.connect("clicked", self.on_click_play_button)

    def get_entry_text(self):
        """Retrieve the content of the entry filter

        Returns
        -------
        str
            The text writed by the user in the filter entry
        """

        return self.searchentry.get_text()

    def get_filter_states(self):
        """Retrieve the check state from the filters

        Returns
        -------
        dict
            The check state of each filter flags as dict structure
        """

        states = dict()
        for flag in self.FILTERS_LIST:
            variant = self.interface.actions.get(flag).get_state()
            states[flag.removeprefix("filter_")] = variant.get_boolean()

        return states

    def on_change_sort_column(self, action, variant):
        """Update the sorted column based on the menu selection

        Parameters
        ----------
        action : gi.repository.Gio.SimpleAction
            The action which received the signal
        variant : gi.repository.GLib.Variant
            The new value for the state
        """

        sort_column = variant.get_uint16()

        # Only update if the user do not select the same option again
        if not action.get_state().get_uint16() == sort_column:
            action.set_state(variant)

            self.interface.set_games_sorted_column(sort_column)

            match sort_column:
                case GamesColumns.FAVORITE:
                    value = "favorite"
                case GamesColumns.FINISHED:
                    value = "finished"
                case GamesColumns.MULTIPLAYER:
                    value = "multiplayer"
                case _:
                    value = "label"

            self.config.update_option("games", "sorted-by", value)

    def on_change_sort_order(self, action, variant):
        """Update the sorted order based on the menu selection

        Parameters
        ----------
        action : gi.repository.Gio.SimpleAction
            The action which received the signal
        variant : gi.repository.GLib.Variant
            The new value for the state
        """

        sort_order = variant.get_uint16()

        # Only update if the user do not select the same option again
        if not action.get_state().get_uint16() == sort_order:
            action.set_state(variant)

            self.interface.set_games_sorted_order(sort_order)

            match sort_order:
                case Gtk.SortType.ASCENDING:
                    value = "ascending"
                case _:
                    value = "descending"

            self.image_sort.set_from_icon_name(
                f"view-sort-{value}-symbolic", Gtk.IconSize.BUTTON
            )

            self.config.update_option("games", "sorted-order", value)

    def on_click_play_button(self, *args):
        """Start or stop the selected game based on its running status"""

        self.application.alternate_game_state(self.interface.selected_game)

    @block_interface_signals
    def on_reset_filters(self, *args):
        """Reset the filters from the popover with their initial state"""

        for flag in self.FILTERS_LIST:
            self.interface.set_state_for_action(flag, state=True)

        self.interface.games.invalidate()

    def on_update_filter(self, action, variant):
        """Update the checked filter from the filters Popover

        Parameters
        ----------
        action : gi.repository.Gio.SimpleAction
            The action which received the signal
        variant : gi.repository.GLib.Variant
            The new value for the state
        """

        action.set_state(variant)

        self.interface.games.invalidate()

    def reset(self):
        """Reset the widgets inside the headerbar"""

        self.button_properties.set_sensitive(False)
        self.button_play.set_sensitive(False)
        self.button_screenshots.set_sensitive(False)
        self.button_log.set_sensitive(False)
        self.button_notes.set_sensitive(False)

    def set_filtering_widgets(self, state):
        """Define the sensitive state of the filtering and sorting widgets

        Parameters
        ----------
        state : bool
            Set the sensitive state of the widgets
        """

        self.searchentry.set_sensitive(state)
        self.button_filters.set_sensitive(state)
        self.button_sort.set_sensitive(state)

    def toggle_button_play(self, is_started):
        """Toggle the state of the playing button based on the game status

        Parameters
        ----------
        is_started : bool
             True if the game is currently running, False otherwise
        """

        media_action = "stop" if is_started else "start"

        self.image_play.set_from_icon_name(
            f"media-playback-{media_action}-symbolic", Gtk.IconSize.BUTTON
        )

        self.button_play.set_label(_("Stop") if is_started else _("Play"))

        context = self.button_play.get_style_context()

        if is_started:
            context.remove_class(Gtk.STYLE_CLASS_SUGGESTED_ACTION)
            context.add_class(Gtk.STYLE_CLASS_DESTRUCTIVE_ACTION)
        else:
            context.remove_class(Gtk.STYLE_CLASS_DESTRUCTIVE_ACTION)
            context.add_class(Gtk.STYLE_CLASS_SUGGESTED_ACTION)

    def update(self, game, launcher):
        """Update the headerbar widgets from a specific game

        Parameters
        ----------
        game : geode_gem.core.game.Game
            The instance of the game used to retrieve information
        """

        self.reset()

        self.button_properties.set_sensitive(True)

        self.button_play.set_sensitive(True)
        self.toggle_button_play(self.application.check_game_is_started(game))

        if launcher is not None and len(launcher.get_screenshots(game)) > 0:
            self.button_screenshots.set_sensitive(True)

        if game.log_path.exists():
            self.button_log.set_sensitive(True)

        self.button_notes.set_sensitive(True)
