# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from gi.repository import Gdk, GdkPixbuf, GLib, Gtk

from geode_gem.interface.utils import add_style_class_to_widget, PixbufSize


class GeodeGemScreenshots(Gtk.Dialog):
    """Visualize screenshots from the selected game

    Attributes
    ----------
    game : geode_gem.core.game.GenericGame
        The instance of the selected game
    index : int
        The current index of the visible screenshot
    interface : geode_gem.interface.GeodeGemInterface
        The instance of the parent window
    pixbufs : list
        The list of original pixbuf for each screenshots
    screenshots : list
        The list of screenshots from the selected game
    zoom_level : int
        The level of zoom used to scale the visible screenshot

    See Also
    --------
    gi.repository.Gtk.Dialog
    """

    DEFAULT_IMAGE_SIZE = 800

    MAXIMAL_ZOOM = 400
    MINIMAL_ZOOM = 10
    STARTUP_ZOOM = 200

    PAGE_INCREMENT = 10
    STEP_INCREMENT = 5

    def __init__(self, interface, **kwargs):
        """Constructor

        Parameters
        ----------
        interface : geode_gem.interface.GeodeGemWindow
            The instance of the window linked to the main application

        See Also
        --------
        gi.repository.Gtk.Dialog
        """

        super().__init__(
            destroy_with_parent=True,
            transient_for=interface,
            use_header_bar=True,
            **kwargs,
        )

        self.game = interface.selected_game
        self.launcher = interface.selected_launcher
        self.screenshots = self.launcher.get_screenshots(self.game)

        self.interface = interface

        self.index = int()
        self.zoom_level = self.STARTUP_ZOOM

        self.pixbufs = list()
        for path in self.screenshots:
            try:
                pixbuf = GdkPixbuf.Pixbuf.new_from_file(str(path))
            except GLib.Error:
                pixbuf = None

            self.pixbufs.append(pixbuf)

        self.__init_widgets__()
        self.__packing_widgets__()
        self.__connect_widgets__()

        self.show_all()
        self.update()

    def __init_widgets__(self):
        """Initialize the interface widgets"""

        self.headerbar = self.get_header_bar()
        self.headerbar.set_title(self.game.label)

        self.grid_navigation = Gtk.Box()
        self.button_first = Gtk.Button(
            image=Gtk.Image(
                icon_name="go-first-symbolic", icon_size=Gtk.IconSize.BUTTON
            ),
            sensitive=False,
        )
        self.button_previous = Gtk.Button(
            image=Gtk.Image(
                icon_name="go-previous-symbolic", icon_size=Gtk.IconSize.BUTTON
            ),
            sensitive=False,
        )
        self.button_next = Gtk.Button(
            image=Gtk.Image(
                icon_name="go-next-symbolic", icon_size=Gtk.IconSize.BUTTON
            ),
            sensitive=False,
        )
        self.button_last = Gtk.Button(
            image=Gtk.Image(
                icon_name="go-last-symbolic", icon_size=Gtk.IconSize.BUTTON
            ),
            sensitive=False,
        )

        self.grid_zoom = Gtk.Box()
        self.button_zoom_fit = Gtk.ToggleButton(
            active=True,
            image=Gtk.Image(
                icon_name="zoom-fit-best-symbolic",
                icon_size=Gtk.IconSize.BUTTON,
            ),
        )
        self.button_zoom_original = Gtk.ToggleButton(
            image=Gtk.Image(
                icon_name="zoom-original-symbolic",
                icon_size=Gtk.IconSize.BUTTON,
            )
        )

        self.overlay_screenshot = Gtk.Overlay()

        self.scroll_screenshot = Gtk.ScrolledWindow()
        self.scroll_screenshot.drag_source_set(
            Gdk.ModifierType.BUTTON1_MASK, list(), Gdk.DragAction.COPY
        )
        self.scroll_screenshot.drag_source_add_uri_targets()

        self.button_overlay_previous = Gtk.Button(
            image=Gtk.Image(
                icon_name="go-previous-symbolic", icon_size=Gtk.IconSize.BUTTON
            ),
            margin_bottom=6,
            margin_end=6,
            margin_start=6,
            margin_top=6,
            no_show_all=True,
            halign=Gtk.Align.START,
            valign=Gtk.Align.CENTER,
        )
        self.button_overlay_next = Gtk.Button(
            image=Gtk.Image(
                icon_name="go-next-symbolic", icon_size=Gtk.IconSize.BUTTON
            ),
            margin_bottom=6,
            margin_end=6,
            margin_start=6,
            margin_top=6,
            no_show_all=True,
            halign=Gtk.Align.END,
            valign=Gtk.Align.CENTER,
        )

        self.scale_overlay_zoom = Gtk.Scale(
            adjustment=Gtk.Adjustment(
                lower=self.MINIMAL_ZOOM,
                upper=self.MAXIMAL_ZOOM,
                page_increment=self.PAGE_INCREMENT,
                step_increment=self.STEP_INCREMENT,
            ),
            draw_value=False,
            halign=Gtk.Align.CENTER,
            margin_bottom=6,
            margin_end=6,
            margin_start=6,
            margin_top=6,
            orientation=Gtk.Orientation.HORIZONTAL,
            valign=Gtk.Align.END,
        )
        self.scale_overlay_zoom.set_size_request(150, -1)

        self.image_screenshot = Gtk.Image(
            icon_name="image-missing-symbolic", icon_size=Gtk.IconSize.DIALOG
        )

        for mark in (self.MINIMAL_ZOOM, self.MAXIMAL_ZOOM, self.STARTUP_ZOOM):
            self.scale_overlay_zoom.add_mark(
                mark, Gtk.PositionType.BOTTOM, None
            )

        for box in (self.grid_navigation, self.grid_zoom):
            add_style_class_to_widget(Gtk.STYLE_CLASS_LINKED, box)

        for button in (self.button_overlay_next, self.button_overlay_previous):
            add_style_class_to_widget(Gtk.STYLE_CLASS_OSD, button)

    def __connect_widgets__(self):
        """Connect Gtk signal to the interface widgets"""

        self.connect("key-press-event", self.on_key_press_event)

        self.scroll_screenshot.connect("drag-data-get", self.on_drag_data_get)

        for button in (
            self.button_first,
            self.button_previous,
            self.button_next,
            self.button_last,
            self.button_overlay_previous,
            self.button_overlay_next,
        ):
            button.connect("clicked", self.on_navigate_screenshot)

        for button in (self.button_zoom_fit, self.button_zoom_original):
            button.connect("clicked", self.on_select_zoom_mode)

        self.scale_overlay_zoom.connect("change-value", self.on_update_scale)

    def __packing_widgets__(self):
        """Packing together the interface widgets"""

        self.grid_navigation.add(self.button_first)
        self.grid_navigation.add(self.button_previous)
        self.grid_navigation.add(self.button_next)
        self.grid_navigation.add(self.button_last)

        self.grid_zoom.add(self.button_zoom_fit)
        self.grid_zoom.add(self.button_zoom_original)

        self.headerbar.pack_start(self.grid_navigation)
        self.headerbar.pack_end(self.grid_zoom)

        self.overlay_screenshot.add_overlay(self.button_overlay_next)
        self.overlay_screenshot.add_overlay(self.button_overlay_previous)
        self.overlay_screenshot.add_overlay(self.scale_overlay_zoom)
        self.overlay_screenshot.add(self.scroll_screenshot)
        self.scroll_screenshot.add(self.image_screenshot)

        self.grid = self.get_content_area()
        self.grid.pack_start(self.overlay_screenshot, True, True, 0)

    def decrement_index(self):
        """Decrement the index of the visible screenshot"""

        self.index -= 1
        if self.index < 0:
            self.index = 0

    def decrement_zoom(self):
        """Decrement the zoom level of the visible screenshot"""

        self.zoom_level -= self.STEP_INCREMENT
        if self.zoom_level < self.MINIMAL_ZOOM:
            self.zoom_level = self.MINIMAL_ZOOM

    def get_size_from_pixbuf(self, pixbuf):
        """Retrieve the size object from the specified pixbuf

        Parameters
        ----------
        pixbuf : GdkPixbuf.Pixbuf
            The instance of the pixbuf used to generate the PixbufSize

        Returns
        -------
        geode_gem.interface.utils.PixbufSize
            The generate PixbufSize object
        """

        return PixbufSize(pixbuf, self.DEFAULT_IMAGE_SIZE)

    def increment_index(self):
        """Increment the index of the visible screenshot"""

        self.index += 1
        if self.index >= len(self.screenshots):
            self.index = len(self.screenshots) - 1

    def increment_zoom(self):
        """Increment the zoom level of the visible screenshot"""

        self.zoom_level += self.STEP_INCREMENT
        if self.zoom_level >= self.MAXIMAL_ZOOM:
            self.zoom_level = self.MAXIMAL_ZOOM

    def on_drag_data_get(self, widget, context, data, info, time):
        """Send the visible screenshot path to the drag context

        Parameters
        ----------
        widget : gi.repository.Gtk.Widget
            The object which receive the signal
        context : gi.repository.Gdk.DragContext
            The drag context
        data : gi.repository.Gtk.SelectionData
            The selection to be filled with the dragged data
        info : int
            The info that has been registered with the target
        time : int
            The timestamp at which the data was received
        """

        path = self.screenshots[self.index]
        if path.exists():
            data.set_uris([f"file://{path}"])

    def on_key_press_event(self, widget, event):
        """Update the screenshot by using the keyboard

        Parameters
        ----------
        widget : gi.repository.Gtk.Widget
            The object which received the signal
        event : gi.repository.Gdk.EventKey
            The event which triggered this signal
        """

        if event.keyval == Gdk.KEY_Left:
            self.decrement_index()
        elif event.keyval == Gdk.KEY_Right:
            self.increment_index()
        elif event.keyval == Gdk.KEY_KP_Subtract:
            self.decrement_zoom()
        elif event.keyval == Gdk.KEY_KP_Add:
            self.increment_zoom()

        self.update()

    def on_navigate_screenshot(self, button):
        """Update the screenshot by using the navigation buttons

        Parameters
        ----------
        button : gi.repository.Gtk.Button
            The instance of the button which was clicked by the user
        """

        if button == self.button_first:
            self.index = 0
        elif button in (self.button_previous, self.button_overlay_previous):
            self.decrement_index()
        elif button in (self.button_next, self.button_overlay_next):
            self.increment_index()
        elif button == self.button_last:
            self.index = len(self.screenshots) - 1

        self.update()

    def on_select_zoom_mode(self, button):
        """Update the zoom level from the selected zoom level

        Parameters
        ----------
        button : gi.repository.Gtk.Button
            The instance of the button which was clicked by the user
        """

        if button == self.button_zoom_fit and button.get_active():
            self.button_zoom_original.set_active(False)
        elif button == self.button_zoom_original and button.get_active():
            self.button_zoom_fit.set_active(False)

        self.set_screenshot()

    def on_update_scale(self, widget, scroll, value):
        """Update the screenshot zoom by using the scale widget

        Parameters
        ----------
        widget : gi.repository.Gtk.Range
            The object which receive signal
        scroll : gi.repository.Gtk.ScrollType
            The type of scroll action that was performed
        value : float
            The new value resulting from the scroll action

        Returns
        -------
        bool
            True to prevent other handlers from being invoked for the signal,
            False to propagate the signal further
        """

        self.zoom_level = int(value)

        if self.zoom_level < self.MINIMAL_ZOOM:
            self.zoom_level = self.MINIMAL_ZOOM
        elif self.zoom_level >= self.MAXIMAL_ZOOM:
            self.zoom_level = self.MAXIMAL_ZOOM

        self.button_zoom_fit.set_active(False)
        self.button_zoom_original.set_active(False)

        self.set_screenshot()

        return False

    def refresh_buttons(self):
        """Refresh the toolbar buttons visibility"""

        has_previous = self.index > 0
        has_next = self.index < len(self.screenshots) - 1

        self.button_first.set_sensitive(has_previous)
        self.button_previous.set_sensitive(has_previous)
        self.button_next.set_sensitive(has_next)
        self.button_last.set_sensitive(has_next)

        if has_previous:
            self.button_overlay_previous.show()
        else:
            self.button_overlay_previous.hide()

        if has_next:
            self.button_overlay_next.show()
        else:
            self.button_overlay_next.hide()

    def set_screenshot(self):
        """Define the image widget from the current index and zoom level"""

        path = self.screenshots[self.index]
        pixbuf = self.pixbufs[self.index]

        if not path.exists() or pixbuf is None:
            self.image_screenshot.set_from_icon_name(
                "image-missing-symbolic", Gtk.IconSize.DIALOG
            )

        else:
            size = self.get_size_from_pixbuf(pixbuf)
            self.refresh_zoom_level(size)

            width = int((self.zoom_level * size.default_width) / 100)
            height = int((self.zoom_level * size.default_height) / 100)

            self.image_screenshot.set_from_pixbuf(
                pixbuf.scale_simple(width, height, GdkPixbuf.InterpType.TILES)
            )

    def refresh_zoom_level(self, size):
        """Update the zoom level from selected zoom mode

        Parameters
        ----------
        size : geode_gem.interface.utils.PixbufSize
            The instance of the PixbufSize from the visible screenshot
        """

        if self.button_zoom_fit.get_active():
            allocation = self.scroll_screenshot.get_allocation()
            ratio = size.get_ratio(allocation.width, allocation.height)
            self.zoom_level = int(min(*ratio) * 100)

        elif self.button_zoom_original.get_active():
            ratio = size.get_ratio(*size.size)
            self.zoom_level = int(min(*ratio) * 100)

        self.scale_overlay_zoom.set_value(float(self.zoom_level))

    def update(self, *args, **kwargs):
        """Update the buttons and the visible screenshot"""

        self.refresh_buttons()

        self.set_screenshot()
