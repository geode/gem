# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from gettext import gettext as _

from gi.repository import Gtk


class Revealer(Gtk.Revealer):
    def __init__(self, interface, *args, **kwargs):
        """Constructor

        Parameters
        ----------
        interface : geode_gem.interface.GeodeGemInterface
            The instance of the main interface
        """

        super().__init__(**kwargs)

        self.interface = interface

        self.grid = Gtk.Box(spacing=12)

        self.label = Gtk.Label(
            label=_("Games Loading in Progress..."),
            halign=Gtk.Align.CENTER,
            margin_bottom=12,
            margin_start=18,
            margin_end=6,
            margin_top=12,
            valign=Gtk.Align.CENTER,
        )

        self.spinner = Gtk.Spinner(
            margin_bottom=12, margin_start=6, margin_end=18, margin_top=12
        )

        self.grid.add(self.label)
        self.grid.add(self.spinner)

        self.add(self.grid)

        context = self.grid.get_style_context()
        context.add_class(Gtk.STYLE_CLASS_OSD)
