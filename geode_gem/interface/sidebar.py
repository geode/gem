# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from gettext import gettext as _
from typing import NoReturn, Self

from gi.repository import Gtk, GdkPixbuf, Pango

from geode_gem.core.utils import get_creation_datetime
from geode_gem.interface.utils import (
    add_style_class_to_widget,
    get_pixbuf_from_name,
    replace_for_markup,
    pretty_timedelta,
)


class Sidebar(Gtk.Grid):
    def __init__(self, interface, *args, **kwargs):
        """Constructor

        Parameters
        ----------
        interface : geode_gem.interface.GeodeGemWindow
            The instance of the window linked to the main application

        See Also
        --------
        gi.repository.Gtk.Grid
        """

        super().__init__(border_width=18, column_spacing=12, row_spacing=18)

        self.interface = interface
        self.icon_mode = None

        self.grid_flags = Gtk.Box(spacing=12)
        self.grid_launcher = Gtk.Box(halign=Gtk.Align.START, spacing=6)
        self.grid_icons = Gtk.Box()
        self.grid_information = Gtk.Grid(
            column_spacing=12, row_spacing=6, vexpand=True
        )

        self.title = Gtk.Label(
            ellipsize=Pango.EllipsizeMode.END, hexpand=True, use_markup=True
        )

        self.finished = Gtk.Image(
            icon_name="emblem-ok-symbolic",
            icon_size=Gtk.IconSize.LARGE_TOOLBAR,
            no_show_all=True,
        )
        self.multiplayer = Gtk.Image(
            icon_name="avatar-default-symbolic",
            icon_size=Gtk.IconSize.LARGE_TOOLBAR,
            no_show_all=True,
        )
        self.screenshots = Gtk.Image(
            icon_name="camera-photo-symbolic",
            icon_size=Gtk.IconSize.LARGE_TOOLBAR,
            no_show_all=True,
        )
        self.savestates = Gtk.Image(
            icon_name="media-floppy-symbolic",
            icon_size=Gtk.IconSize.LARGE_TOOLBAR,
            no_show_all=True,
        )

        self.separator_launcher = Gtk.Separator()
        self.image_launcher = Gtk.Image(
            icon_name="input-gaming-symbolic",
            icon_size=Gtk.IconSize.LARGE_TOOLBAR,
            no_show_all=True,
        )
        self.label_launcher = Gtk.Label(no_show_all=True)

        self.icon = Gtk.Image(valign=Gtk.Align.START)
        self.screenshot = Gtk.Image(no_show_all=True, valign=Gtk.Align.START)

        self.played = Gtk.Label(label=_("Played"), no_show_all=True, xalign=1)
        self.played_value = Gtk.Label(
            hexpand=True, ellipsize=Pango.EllipsizeMode.END, xalign=0
        )

        self.time_played = Gtk.Label(
            label=_("Time Played"), no_show_all=True, xalign=1
        )
        self.time_played_value = Gtk.Label(
            hexpand=True, ellipsize=Pango.EllipsizeMode.END, xalign=0
        )

        self.last_played = Gtk.Label(
            label=_("Last Played"), no_show_all=True, xalign=1
        )
        self.last_played_value = Gtk.Label(
            hexpand=True, ellipsize=Pango.EllipsizeMode.END, xalign=0
        )

        self.grid_icons.add(self.icon)
        self.grid_icons.add(self.screenshot)

        self.grid_launcher.add(self.image_launcher)
        self.grid_launcher.add(self.label_launcher)

        self.grid_flags.add(self.multiplayer)
        self.grid_flags.add(self.finished)
        self.grid_flags.add(self.screenshots)
        self.grid_flags.add(self.savestates)
        self.grid_flags.add(self.separator_launcher)
        self.grid_flags.add(self.grid_launcher)

        self.grid_information.attach(self.played, 0, 0, 1, 1)
        self.grid_information.attach(self.played_value, 1, 0, 1, 1)
        self.grid_information.attach(self.time_played, 0, 1, 1, 1)
        self.grid_information.attach(self.time_played_value, 1, 1, 1, 1)
        self.grid_information.attach(self.last_played, 0, 2, 1, 1)
        self.grid_information.attach(self.last_played_value, 1, 2, 1, 1)

        for label in (
            self.played,
            self.time_played,
            self.last_played,
        ):
            add_style_class_to_widget(Gtk.STYLE_CLASS_DIM_LABEL, label)

        self.reset()

    def reset(self):
        """Reset the widgets inside the sidebar"""

        self.show_all()

        self.title.set_markup("")

        self.icon.hide()
        self.screenshot.hide()

        self.finished.hide()
        self.multiplayer.hide()
        self.savestates.hide()
        self.screenshots.hide()

        self.separator_launcher.hide()
        self.image_launcher.hide()
        self.label_launcher.hide()

        self.played.hide()
        self.played_value.set_label("")

        self.time_played.hide()
        self.time_played_value.set_label("")

        self.last_played.hide()
        self.last_played_value.set_label("")

    def set_icon_mode(self: Self, icon_mode: Gtk.ImageType) -> NoReturn:
        """Define the visibility of the game icon inside the sidebar

        Parameters
        ----------
        icon_mode : gi.repository.Gtk.ImageType
            The mode of the game icon visibility
        """

        self.icon_mode = icon_mode

    def set_orientation(self, orientation):
        """Define the orientation of the widgets inside the sidebar

        Parameters
        ----------
        position : gi.repository.Gtk.Orientation
            The orientation of the widgets
        """

        for child in self.get_children():
            self.remove(child)

        if orientation == Gtk.Orientation.VERTICAL:
            self.title.set_xalign(0)

            self.screenshot.set_halign(Gtk.Align.END)

            self.grid_flags.set_halign(Gtk.Align.START)
            self.grid_icons.set_halign(Gtk.Align.END)
            self.grid_information.set_halign(Gtk.Align.START)

            self.attach(self.title, 0, 0, 1, 1)
            self.attach(self.grid_information, 0, 1, 1, 1)
            self.attach(self.grid_flags, 0, 2, 1, 1)
            self.attach(self.grid_icons, 1, 0, 1, 3)

        elif orientation == Gtk.Orientation.HORIZONTAL:
            self.title.set_xalign(0.5)

            self.screenshot.set_halign(Gtk.Align.CENTER)

            self.grid_flags.set_halign(Gtk.Align.END)
            self.grid_icons.set_halign(Gtk.Align.CENTER)
            self.grid_information.set_halign(Gtk.Align.CENTER)

            self.attach(self.title, 0, 0, 1, 1)
            self.attach(self.grid_icons, 0, 1, 1, 1)
            self.attach(self.grid_information, 0, 2, 1, 1)
            self.attach(self.grid_flags, 0, 3, 1, 1)

    def update(self, game, launcher=None):
        """Update the sidebar widgets from a specific game

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game used to retrieve information
        """

        self.reset()

        self.played.show()
        self.time_played.show()
        self.last_played.show()

        self.title.set_markup(
            f"<big><b>{replace_for_markup(game.label)}</b></big>"
        )

        if game.finished:
            self.finished.show()

        self.multiplayer.show()
        if game.multiplayer:
            self.multiplayer.set_tooltip_text(_("Multiplayer"))
            self.multiplayer.set_from_icon_name(
                "system-users-symbolic", Gtk.IconSize.LARGE_TOOLBAR
            )

        else:
            self.multiplayer.set_tooltip_text(_("Singleplayer"))
            self.multiplayer.set_from_icon_name(
                "avatar-default-symbolic", Gtk.IconSize.LARGE_TOOLBAR
            )

        icon = None
        if launcher is not None:
            self.separator_launcher.show()
            self.image_launcher.show()
            self.label_launcher.show()

            icon_name = launcher.icon
            if not self.interface.icons_theme.has_icon(icon_name):
                icon_name = "input-gaming-symbolic"

            self.image_launcher.set_from_icon_name(
                icon_name, Gtk.IconSize.LARGE_TOOLBAR
            )

            self.label_launcher.set_label(launcher.label)
            self.grid_launcher.set_tooltip_text(launcher.label)

            if (
                self.icon_mode == Gtk.ImageType.PIXBUF
                and len(screenshots := launcher.get_screenshots(game)) > 0
            ):
                path = screenshots[-1]

                icon = GdkPixbuf.Pixbuf.new_from_file_at_scale(
                    str(path), 300, 200, True
                )
                if icon is not None:
                    self.screenshot.set_tooltip_text(
                        get_creation_datetime(path).strftime(
                            _("Screenshot taken the %x")
                        )
                    )
                    self.screenshot.set_from_pixbuf(icon)
                    self.screenshot.show()

        if icon is None and not self.icon_mode == Gtk.ImageType.EMPTY:
            icon = get_pixbuf_from_name(
                self.interface.icons_theme,
                game.icon,
                fallback_name=game.collection.icon,
                size=200,
            )
            if icon is not None:
                self.icon.set_from_pixbuf(icon)
                self.icon.show()

        time_played = _("N/A")
        if (
            game.time_played is not None
            and game.time_played.total_seconds() > 0
        ):
            time_played = pretty_timedelta(game.time_played)

        last_played = _("Never")
        if game.last_played is not None:
            last_played = game.last_played.strftime("%x")

            if (
                game.last_played_time is not None
                and game.last_played_time.total_seconds() > 0
            ):
                last_played += f" ( {pretty_timedelta(game.last_played_time)} )"

        self.played_value.set_label(str(game.played))
        self.time_played_value.set_label(time_played)
        self.last_played_value.set_label(last_played)
