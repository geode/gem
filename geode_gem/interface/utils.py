# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from datetime import date, datetime, time, timedelta
from gettext import gettext as _, ngettext
from os import R_OK, W_OK, access
from pathlib import Path
from re import match as re_match, escape as re_escape, findall as re_findall
from subprocess import PIPE, Popen, STDOUT
from typing import Tuple

from gi.repository import GdkPixbuf, Gio, GLib, Gtk

from geode_gem.core.utils import get_binary_path


def add_style_class_to_widget(style_class, widget):
    """Add a style class to the specified widget

    Parameters
    ----------
    style_class : str
        The name of the style class to apply on widget
    widget : gi.repository.Gtk.Widget
        The instance of the widget to use in styling

    See Also
    --------
    gi.repository.Gtk.StyleContext
    """

    widget.get_style_context().add_class(style_class)


def block_interface_signals(function):
    """Decorator to manage signals blocking behavior"""

    def __function__(self, *args, **kwargs):
        interface = self

        # Signals only occurs on the main interface instance
        if hasattr(self, "interface"):
            interface = self.interface

        interface.block_signals()
        results = function(self, *args, **kwargs)
        interface.unblock_signals()

        return results

    return __function__


def call_external_application(*command):
    """Call an external application from operating system

    Parameters
    ----------
    command : list
        Command execution parameters as string list

    Returns
    -------
    str
        Splited output if executed, None otherwise
    """

    if get_binary_path(command[0]):
        proc = Popen(
            command,
            stdin=PIPE,
            stdout=PIPE,
            stderr=STDOUT,
            universal_newlines=True,
        )

        output, error_output = proc.communicate()
        if output:
            return output.split("\n")[0]

    return None


def check_game_is_editable(game):
    """Check if specified game is a text file and can be edited by user

    Parameters
    ----------
    game : geode_gem.engine.game.Game
        Game instance

    Returns
    -------
    bool
        True if game file is editable, False otherwise
    """

    if not magic_from_file(game.path, mime=True).startswith("text/"):
        return False

    return access(game.path, R_OK) and access(game.path, W_OK)


def check_mednafen():
    """Check if Mednafen exists on user system

    This function read the first line of mednafen default output and check
    if this one match "Starting Mednafen [0-9+.?]+".

    Returns
    -------
    bool
        Mednafen exists status

    Notes
    -----
    Still possible to troll this function with a script call mednafen which
    send the match string as output. But, this problem only appear if a
    user want to do that, so ...
    """

    output = call_external_application("mednafen")

    return output and re_match(
        r"Starting Mednafen [\d+\.?]+", output.split("\n")[0]
    )


def convert_state_to_glib_variant(state):
    """Convert a state to a specific GLib variant

    Parameters
    ----------
    state : gi.repository.GLib.Variant or type
        The state to convert in a GLib variant

    Returns
    -------
    gi.repository.GLib.Variant
        The converted state as GLib variant instance
    """

    if isinstance(state, bool):
        return GLib.Variant.new_boolean(state)

    if isinstance(state, int):
        return GLib.Variant.new_uint16(state)

    return state


def generate_blank_icons():
    """Generate multiple blank icons to use as empty Pixbuf

    Returns
    -------
    dict
        Icons storage
    """

    storage = dict()

    for size in (8, 16, 22, 24, 32, 48, 64, 96):
        storage[size] = GdkPixbuf.Pixbuf.new(
            GdkPixbuf.Colorspace.RGB, True, 8, size, size
        )
        storage[size].fill(0x00000000)

    return storage


def generate_radio_menuitem(label, action, variant):
    """Generate a MenuItem with a radio button

    Parameters
    ----------
    label : str
        The label of the item in the menu
    action : str
        The identifier of the action for widget connection
    variant : gi.repository.GLib.Variant
        The variant to use as value for the menu item

    Returns
    -------
    gi.repository.Gio.MenuItem
        The generated menu item instance
    """

    menu_item = Gio.MenuItem.new(label, action)
    menu_item.set_attribute_value(Gio.MENU_ATTRIBUTE_TARGET, variant)

    return menu_item


def get_filename_from_icon_theme(icons_theme, name, size):
    """Retrieve filepath from a specific name in icon theme

    Parameters
    ----------
    icons_theme : gi.repository.Gtk.IconsTheme
        Gtk Icons Theme object instance
    name : str
        Icon name to search in icon theme
    size : int
        Icon size in pixels

    Returns
    -------
    str or None
        Icon filepath if found, None otherwise
    """

    icon_info = icons_theme.lookup_icon(
        name, size, Gtk.IconLookupFlags.FORCE_SIZE
    )

    if icon_info is not None:
        return icon_info.get_filename()

    return None


def get_path_from_string(filepath: str, fallback: str = "~") -> Path:
    """Retrieve a Path object from the specified path

    Parameters
    ----------
    filepath : str
        The string which contains the path to the file
    fallback : str, optional
        The fallback string to use if the specified file is empty or do not
        exists

    Returns
    -------
    pathlib.Path
        The instance of the Path object related to the specified parameters
    """

    path = Path(filepath).expanduser()
    if len(filepath.strip()) == 0 or not path.exists():
        path = Path(fallback).expanduser()

    return path


def get_pixbuf_from_name(icons_theme, name, fallback_name=None, size=64):
    """Retrieve a pixbuf object from an icon name

    Parameters
    ----------
    icons_theme : gi.repository.Gtk.IconsTheme
        Gtk Icons Theme object instance
    name : str
        The name of the icon to generate
    fallback_name : str, optional
        The name of the fallback icon
    size : int, optional
        The size of the icon in pixels

    Returns
    -------
    GdkPixbuf.Pixbuf
        The generated Pixbuf object for the specified icon name
    """

    if name is None or len(name.strip()) == 0:
        name = fallback_name
        if name is None:
            return None

    path = Path(name).expanduser()
    if not path.exists() or path.is_dir():
        path = get_filename_from_icon_theme(icons_theme, name, size)
        if path is not None:
            path = Path(path).expanduser()

    if path is None or not path.exists() or path.is_dir():
        path = get_filename_from_icon_theme(icons_theme, fallback_name, size)
        if path is None:
            return None

    try:
        return GdkPixbuf.Pixbuf.new_from_file_at_scale(
            str(path), size, size, True
        )
    except Exception:
        return None


def get_today_from_datetime(date_object):
    """Retrieve current time for a specific datetime type

    This function is only used to test string_from_date with unittest.mock

    Parameters
    ----------
    date_object : datetime.datetime
        Date to retrieve datetime type

    Returns
    -------
    datetime.date or datetime.datetime
        Current time
    """

    if isinstance(date_object, datetime):
        return datetime.now()

    return date.today()


def icon_from_data(path, fallback=None, width=24, height=24):
    """Load an icon from path

    This function search if an icon is available in GEM icons folder and return
    a generate Pixbuf from the icon path. If no icon was found, return an empty
    generate Pixbuf.

    Parameters
    ----------
    path : pathlib.Path
        Absolute or relative icon path
    fallback : GdkPixbuf.Pixbuf, optional
        Fallback icon to return instead of empty (Default: None)
    width : int, optional
        Icon width in pixels (Default: 24)
    height : int, optional
        Icon height in pixels (Default: 24)

    Returns
    -------
    GdkPixbuf.Pixbuf
        Pixbuf icon object
    """

    if path.exists():
        try:
            return GdkPixbuf.Pixbuf.new_from_file_at_size(
                str(path), width, height
            )
        except Exception:
            pass

    # Return an empty icon
    if fallback is None:
        fallback = GdkPixbuf.Pixbuf.new(
            GdkPixbuf.Colorspace.RGB, True, 8, width, height
        )
        fallback.fill(0x00000000)

    return fallback


def magic_from_file(filename, mime=False):
    """Fallback function to retrieve file type when python-magic is missing

    Parameters
    ----------
    filename : str
        File path to read
    mime : bool
        Retrieve the file mimetype, otherwise a readable name (Default: False)

    Returns
    -------
    str
        File type result as human readable name or mimetype
    """

    # Use dereference to follow symlink
    commands = ["file", "--dereference", str(filename)]
    if mime:
        commands.insert(1, "--mime-type")

    output = call_external_application(*commands)
    if output:
        output = output.splitlines()[0]

        result = re_findall(rf"^{re_escape(str(filename))}\:\s+(.*)$", output)
        if result:
            return result[0]

    return str()


def on_activate_listboxrow(listbox, row):
    """Activate internal widget when a row has been activated

    Parameters
    ----------
    listbox : Gtk.ListBox
        Object which receive signal
    row : gem.widgets.widgets.ListBoxItem
        Activated row
    """

    widget = row.get_widget()

    if isinstance(widget, Gtk.ComboBox):
        widget.popup()

    elif isinstance(widget, Gtk.Entry):
        widget.grab_focus()

    elif isinstance(widget, Gtk.FileChooserButton):
        widget.activate()

    elif type(widget) in [Gtk.Button, Gtk.FontButton]:
        widget.clicked()

    elif isinstance(widget, Gtk.SpinButton):
        widget.grab_focus()

    elif isinstance(widget, Gtk.Switch):
        widget.set_active(not widget.get_active())


def on_entry_clear(widget, pos, event):
    """Reset an entry widget when secondary icon is clicked

    Parameters
    ----------
    widget : Gtk.Entry
        Entry widget
    pos : Gtk.EntryIconPosition
        Position of the clicked icon
    event : Gdk.EventButton or Gdk.EventKey
        Event which triggered this signal

    Returns
    -------
    bool
        Function state
    """

    if type(widget) is not Gtk.Entry:
        return False

    if pos == Gtk.EntryIconPosition.SECONDARY and widget.get_text():
        widget.set_text(str())

        return True

    return False


def pretty_timedelta(delta: timedelta) -> str:
    """Reprensents the timedelta as an human translated string

    Parameters
    ----------
    delta : datetime.timedelta
        The delta used to generate the string

    Returns
    -------
    str
        The generated string with translation and time localization
    """

    text: str = ""

    if delta.days > 0:
        text = ngettext("%d day, ", "%d days, ", delta.days) % delta.days

    hours, seconds = divmod(delta.seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    text += time(hour=hours, minute=minutes, second=seconds).strftime("%X")

    return text


def replace_for_markup(text: str) -> str:
    """Replace some characters in text for markup compatibility

    Parameters
    ----------
    text : str
        Text to parser

    Returns
    -------
    str
        Replaced text
    """

    characters = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
    }

    # Avoid to raise an UnicodeEncodeError with specific characters
    text = text.encode(errors="replace").decode()

    for key, value in characters.items():
        text = text.replace(key, value)

    return text


def string_from_date(date_object):
    """Convert a datetime to a pretty string

    Get a pretty string from the interval between NOW() and the wanted date

    Parameters
    ----------
    date_object : datetime.datetime
        Date to compare with NOW()

    Returns
    -------
    str or None
        Convert value
    """

    if date_object is None:
        return None

    days = int((get_today_from_datetime(date_object) - date_object).days)

    if days == 0:
        return _("Today")
    elif days == 1:
        return _("Yesterday")
    elif days == -1:
        return _("Tomorrow")
    elif days > 0 and days < 30:
        return _("%d days ago") % abs(days)
    elif days < 0 and days > -30:
        return _("In %d days") % abs(days)

    months = int(days / 30)

    if months == 1:
        return _("Last month")
    elif months == -1:
        return _("Next month")
    elif months > 0 and months < 12:
        return _("%d months ago") % abs(months)
    elif months < 0 and months > -12:
        return _("In %d months") % abs(months)

    years = int(months / 12)

    if years == 1:
        return _("Last year")
    elif years == -1:
        return _("Next year")
    elif years > 1:
        return _("%d years ago") % abs(years)

    return _("In %d years") % abs(years)


def string_from_time(time_object):
    """Convert a time to a pretty string

    Get a pretty string from the interval between NOW() and the wanted date

    Parameters
    ----------
    time_object : datetime.datetime
        Date to compare with NOW()

    Returns
    -------
    str or None
        Convert value
    """

    if time_object is None:
        return None

    hours, minutes, seconds = int(), int(), int()

    if type(time_object) is timedelta:
        hours, seconds = divmod(time_object.seconds, 3600)

        if seconds > 0:
            minutes, seconds = divmod(seconds, 60)

        hours += time_object.days * 24

    if hours == 0 and minutes == 0 and seconds == 0:
        return str()

    elif hours == 0 and minutes == 0:
        return ngettext(_("1 second"), _("%d seconds") % seconds, seconds)

    elif hours == 0:
        return ngettext(_("1 minute"), _("%d minutes") % minutes, minutes)

    return ngettext(_("1 hour"), _("%d hours") % hours, hours)


class PixbufSize:
    """Represent GdkPixbuf.Pixbuf size for the Geode-GEM viewer

    Attributes
    ----------
    height : int
        The pixbuf height value
    width : int
        The pixbuf width value
    default_size : int
        Default size which will be used to determine the default zoom level size
    """

    def __init__(self, pixbuf: GdkPixbuf.Pixbuf, default_size: int):
        """Constructor

        Parameters
        ----------
        pixbuf : GdkPixbuf.Pixbuf
            The pixbuf used as reference
        kwargs : dict
            Dictionary used to override default attributes
        """

        self.width = pixbuf.get_width()
        self.height = pixbuf.get_height()

        self.default_size = default_size

    @property
    def default_height(self) -> int:
        """Retrieve the default height based on default_size attribute

        Returns
        -------
        int
            The default height value for current pixbuf

        Examples
        --------
        >>> PixbufSize.default_height
        600
        """

        if self.width > self.height:
            return int((self.height * self.default_size) / self.width)

        return self.default_size

    @property
    def default_width(self) -> int:
        """Retrieve the default width based on default_size attribute

        Returns
        -------
        int
            The default width value for current pixbuf

        Examples
        --------
        >>> PixbufSize.default_width
        800
        """

        if self.width > self.height:
            return self.default_size

        return int((self.width * self.default_size) / self.height)

    def get_ratio(self, width: int, height: int) -> Tuple[float, float]:
        """Get the ratio where the pixbuf fit in specified width and height

        Parameters
        ----------
        width : int
            Width value to use as reference
        height : int
            Height value to use as reference

        Returns
        -------
        tuple
            Fit pixbuf ratio as tuple of float

        Examples
        --------
        >>> PixbufSize.get_ratio(1024, 768)
        (1.28, 1.28)
        """

        return (
            float(width / self.default_width),
            float(height / self.default_height),
        )

    @property
    def size(self) -> Tuple[int, int]:
        """Retrieve the pixbuf size

        Returns
        -------
        tuple
            The pixbuf size as tuple of integer

        Examples
        --------
        >>> PixbufSize.size
        (800, 600)
        """

        return (self.width, self.height)
