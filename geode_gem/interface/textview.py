# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from gettext import gettext as _

from gi.repository import Gtk

from geode_gem.interface.utils import add_style_class_to_widget


class GeodeGemTextViewer(Gtk.Dialog):
    """Visualize a specific text file for the selected game

    Attributes
    ----------

    See Also
    --------
    gi.repository.Gtk.Dialog
    """

    def __init__(self, interface, path, subtitle, read_only=False, **kwargs):
        """Constructor

        Parameters
        ----------
        interface : geode_gem.interface.GeodeGemWindow
            The instance of the window linked to the main application
        read_only : bool
            Define if the text editor is in read only mode

        See Also
        --------
        gi.repository.Gtk.Dialog
        """

        super().__init__(
            destroy_with_parent=True,
            transient_for=interface,
            use_header_bar=True,
            **kwargs,
        )

        self.path = path
        self.read_only = read_only
        self.subtitle = subtitle

        self.__init_widgets__()
        self.__packing_widgets__()
        self.__connect_widgets__()

        self.show_all()
        self.update()

    def __init_widgets__(self):
        """Initialize the interface widgets"""

        self.headerbar = self.get_header_bar()
        self.headerbar.set_subtitle(self.subtitle)

        self.button_apply = Gtk.Button(label=_("Save"), no_show_all=True)
        add_style_class_to_widget(
            Gtk.STYLE_CLASS_SUGGESTED_ACTION, self.button_apply
        )

        self.scrolled_window = Gtk.ScrolledWindow()

        self.textview = Gtk.TextView(
            bottom_margin=4,
            buffer=Gtk.TextBuffer(),
            cursor_visible=not self.read_only,
            editable=not self.read_only,
            left_margin=4,
            monospace=True,
            right_margin=4,
            top_margin=4,
        )

    def __connect_widgets__(self):
        """Connect Gtk signal to the interface widgets"""

        self.button_apply.connect("clicked", self.on_apply_modification)

    def __packing_widgets__(self):
        """Packing together the interface widgets"""

        self.headerbar.pack_end(self.button_apply)

        self.scrolled_window.add(self.textview)

        self.grid = self.get_content_area()
        self.grid.pack_start(self.scrolled_window, True, True, 0)

    def get_text(self):
        """Retrieve the content of the text buffer

        Returns
        -------
        str
            The full content of the text buffer with the hidden characters
        """

        text_buffer = self.textview.get_buffer()

        return text_buffer.get_text(
            text_buffer.get_start_iter(), text_buffer.get_end_iter(), True
        )

    def on_apply_modification(self, *args, **kwargs):
        """Emit the response type Apply when the user validate the dialog"""

        self.response(Gtk.ResponseType.APPLY)

    def update(self, *args, **kwargs):
        """Update the text buffer"""

        if not self.read_only:
            self.button_apply.show()

        if self.path.exists() and self.path.is_file():
            with self.path.open("r") as file_buffer:
                self.textview.get_buffer().set_text(file_buffer.read())
