# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from gi.repository import Gtk

from geode_gem.core.metadata import Metadata


class GeodeGemAboutDialog(Gtk.AboutDialog):
    def __init__(self):
        """Constructor"""

        super().__init__(
            artists=Metadata.People.ARTISTS,
            authors=Metadata.People.AUTHORS,
            comments=Metadata.COMMENTS,
            copyright=f"Copyleft {Metadata.YEARS} {Metadata.AUTHOR}",
            license_type=Gtk.License.GPL_3_0,
            logo_icon_name=Metadata.Python.APPLICATION,
            program_name=Metadata.NAME,
            version=f"{Metadata.VERSION} ({Metadata.CODENAME})",
            website=Metadata.WEBSITE,
            wrap_license=True,
        )
