# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from importlib.metadata import version


class Metadata:
    """Represents the Metadata of the Geode-Gem application"""

    NAME = "Geode-Gem"
    VERSION = version("geode-gem")
    CODENAME = "Space Fox"

    AUTHOR = "PacMiam"
    YEARS = "2015-2025"
    LICENSE = "GNU/GPLv3+"

    COMMENTS = "Manage your games collection on your favorite GNU/Linux system"
    WEBSITE = "https://geode.kawateam.dev"

    class People:
        """The person who write or test this application"""

        AUTHORS = [
            "PacMiam https://pacmiam.kawateam.dev/",
        ]
        ARTISTS = [
            "PacMiam https://pacmiam.kawateam.dev - Logo (Art Libre)",
            (
                "Evan-Amos https://commons.wikimedia.org/wiki/User:Evan-Amos"
                " - Game consoles (Public Domain)"
            ),
        ]

    class Python:
        """Specific value for Python and the GObject binding"""

        APPLICATION = "geode-gem"
        LOCALIZATION = "geode_gem"
