# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from collections.abc import Iterable
from dataclasses import dataclass, field
from pathlib import Path
from re import match, IGNORECASE
from typing import NoReturn, Self, TYPE_CHECKING

from xdg.BaseDirectory import xdg_data_dirs
from xdg.DesktopEntry import DesktopEntry

from geode_gem.core.configuration import Configuration
from geode_gem.core.entity import Entity
from geode_gem.core.game import (
    GenericGame,
    ConsoleGame,
    NativeGame,
    ScummvmGame,
    WineGame,
)
from geode_gem.core.utils import generate_identifier

if TYPE_CHECKING:
    from geode_gem.core import Core


@dataclass
class GenericCollection(Entity):
    """Represents a generic collection

    Attributes
    ----------
    launcher : str
        The identifier of the launcher associated to this collection
    favorite : bool
        Mark this collection as a favorite one
    games : dict[str, geode_gem.core.game.GenericGame]
        The cache storage for the games with the game’s identifier as key
    """

    launcher: str = ""

    favorite: bool = False

    games: dict[str, GenericGame] = field(default_factory=dict)

    def __contains__(self: Self, identifier: str) -> bool:
        """Check if the collection contains the specified game identifier

        Parameters
        ----------
        identifier: str
            The identifier of the game

        Returns
        -------
        bool
            True if the game exists in the collection, False otherwise
        """

        return identifier in self.games

    def __delitem__(self: Self, identifier: str) -> NoReturn:
        """Remove the specified game from this collection

        Parameters
        ----------
        identifier: str
            The identifier of the game

        Raises
        ------
        KeyError
            When the specified identifier do not exists in this collection
        """

        if identifier not in self.games:
            raise KeyError(
                f"The identifier {identifier} is not available in this "
                "collection"
            )

        del self.games[identifier]

    def __getitem__(self: Self, identifier: str) -> GenericGame | None:
        """Retrieve the game associated with the identifier in this collection

        Parameters
        ----------
        identifier: str
            The identifier of the game

        Returns
        -------
        geode_gem.core.game.GenericGame or None
            The instance of the game if it exists, None otherwise
        """

        return self.games.get(identifier, None)

    def __iter__(self: Self) -> Iterable[Path]:
        """Allows to use the collection as generator

        Returns
        -------
        iterator[geode_gem.core.game.GenericGame]
            Collection as generator object
        """

        return iter(self.games.values())

    def __len__(self: Self) -> int:
        """Retrieve collection files length

        Returns
        -------
        int
            Files number in current collection
        """

        return len(self.games)

    def __setitem__(self: Self, identifier: str, game: GenericGame) -> NoReturn:
        """Add or update a Game in this collection

        Parameters
        ----------
        identifier: str
            The identifier of the game
        game : geode_gem.core.game.GenericGame
            The instance of the game to store in this collection
        """

        self.games[identifier] = game

    def inspect(self: Self, *args) -> NoReturn:
        """Basic list of games generation method

        Since a GenericCollection is not related to a path or something, this
        method will only be used to clear the list of games.
        """

        self.games.clear()

    @staticmethod
    def parse_configuration(parser: Configuration):
        data = Entity.parse_configuration(parser)
        data["favorite"] = parser.getboolean(
            "options", "favorite", fallback=False
        )
        data["launcher"] = parser.get("metadata", "launcher", fallback="")

        return data


@dataclass
class ConsoleCollection(GenericCollection):
    """Represents a collection dedicated to store ROM games

    Attributes
    ----------
    path : pathlib.Path
        The path of the directory which contains the game associated with this
        collection
    recursive : bool
        Mark the collection as recursive to check every subdirectories from the
        directory set in the path attribute
    extensions : list[str]
        The list of extensions used to retrieve the games in the main directory
    ignored_patterns : list[str]
        The list of patterns used to ignore specific files from the main
        directory
    """

    path: Path = None

    recursive: bool = False

    extensions: list[str] = field(default_factory=list)
    ignored_patterns: list[str] = field(default_factory=list)

    def __setitem__(self: Self, identifier: str, game: GenericGame) -> NoReturn:
        """Add or update a Game in this collection

        Parameters
        ----------
        identifier: str
            The identifier of the game
        game : bigemu.core.game.Game
            The instance of the game to store in this collection

        Raises
        ------
        FileNotFoundError
            When the game is not an element of the collection
        KeyError
            When the game extension is not allowed by the collection
        """

        if not game.path.is_relative_to(self.path):
            raise FileNotFoundError("This game is not inside the collection")

        if game.path.suffix not in self.extensions:
            raise KeyError(
                "The extension of this game is not allowed by this collection"
            )

        super().__setitem__(identifier, game)

    def inspect(self: Self, *args) -> NoReturn:
        """Generate the list of game objects for this collection

        Raises
        ------
        FileNotFoundError
            When the main directory do not exists on the filesystem
        NotADirectoryError
            When the path attribute is not a directory
        """

        super().inspect(*args)

        if self.path is None:
            raise ValueError("This collection do not have a path")

        if not self.path.exists():
            raise FileNotFoundError(
                f"Cannot access to '{self.path}': No such file or directory"
            )

        if not self.path.is_dir():
            raise NotADirectoryError(
                f"Cannot list files with '{self.path}': Not a directory"
            )

        if self.recursive:
            files = self.path.rglob("*")
        else:
            files = self.path.iterdir()

        for path in files:
            if (
                path.is_file()
                and self.is_extension_allowed(path)
                and not self.is_ignored(path)
            ):
                game = ConsoleGame(
                    generate_identifier(path), label=path.stem, path=path
                )
                game.collection = self
                self[game.identifier] = game

    def is_extension_allowed(self: Self, path: Path) -> bool:
        """Check if specified file extension can be loaded

        Parameters
        ----------
        path : pathlib.Path
            File path object

        Returns
        -------
        bool
            True is specified path extension is allowed, False otherwise
        """

        return len(self.extensions) == 0 or path.suffix in self.extensions

    def is_ignored(self: Self, path: Path) -> bool:
        """Check if specified file name can be loaded

        Parameters
        ----------
        path : pathlib.Path
            File path object

        Returns
        -------
        bool
            True is specified path name is ignored, False otherwise
        """

        return len(self.ignored_patterns) > 0 and any(
            [
                match(element, path.name, IGNORECASE) is not None
                for element in self.ignored_patterns
            ]
        )

    @staticmethod
    def parse_configuration(parser: Configuration):
        data = GenericCollection.parse_configuration(parser)
        data["extensions"] = parser.getlist("metadata", "extensions")
        data["path"] = parser.getpath("metadata", "path")
        data["recursive"] = parser.getboolean(
            "options", "recursive", fallback=False
        )

        return data


class NativeCollection(GenericCollection):
    """Represents a collection dedicated to store the Desktop files

    Notes
    -----
    The collection will only retrieve the Desktop files which have a category
    related to game. This list is set in the `CATEGORIES` class constant.
    """

    CATEGORIES = [
        "Game",
        "ActionGame",
        "AdventureGame",
        "ArcadeGame",
        "BoardGame",
        "BlocksGame",
        "CardGame",
        "KidsGame",
        "LogicGame",
        "RolePlaying",
        "Shooter",
        "Simulation",
        "SportsGame",
        "StrategyGame",
    ]

    def inspect(self: Self, *args) -> NoReturn:
        """Generate the list of game objects for this collection

        Notes
        -----
        This method will used the path from the xdg.BaseDirectory.xdg_data_dirs
        constant to retrieve all the Desktop entries.

        This list is reversed to ensure the user local applications directory
        is read after the system one. It will ensure local modification are
        read in priority.
        """

        super().inspect(*args)

        for dirname in reversed(xdg_data_dirs):
            path = Path(dirname, "applications")
            if not path.exists():
                continue

            for filename in path.glob("*.desktop"):
                entry = DesktopEntry(filename)

                categories = entry.getCategories()
                if any(map(lambda e: e in self.CATEGORIES, categories)):
                    game = NativeGame.new_from_desktop_entry(entry)
                    game.collection = self
                    self[game.identifier] = game


@dataclass
class ScummvmCollection(GenericCollection):
    """Represents a collection dedicated to store the ScummVM game files

    Notes
    -----
    This collection will used the ScummVM configuration files to manage games

    Attributes
    ----------
    path : pathlib.Path
        The path to the scummvm.ini file which contains the list of registered
        games
    """

    path: Path = None

    def inspect(self: Self, *args) -> NoReturn:
        """Generate the list of game objects for this collection

        Notes
        -----
        The method used the `~/.config/scummvm/scummvm.ini` from scummvm to
        retrieve the game files. This file can be set at another place, but
        the content must follow the `scummvm.ini` specification.
        """

        super().inspect(*args)

        if self.path.exists():
            parser = Configuration(self.path)

            for section, values in parser.items():
                # Ignore the global section or the DEFAULT from ConfigParser
                if section in ("DEFAULT", "scummvm"):
                    continue

                game = ScummvmGame.new_from_data(section, **dict(values))
                game.collection = self
                self[game.identifier] = game

    @staticmethod
    def parse_configuration(parser: Configuration):
        data = GenericCollection.parse_configuration(parser)
        data["path"] = parser.getpath("metadata", "configuration")

        return data


class WineCollection(GenericCollection):
    """Represents a collection dedicated to store the WINE specification files

    Notes
    -----
    A dedicated specification have been designed to represent a WINE game by
    using a file with a `.ini` extension.

    This file must follow the following format:
    ```ini
    [metadata]
    executable = /tmp/pignouf.exe
    prefix = /tmp/pignouf/
    launcher = wine-system

    [options]
    debug = false
    ```
    """

    def inspect(self: Self, core: "Core") -> NoReturn:
        """Generate the list of game objects for this collection

        Notes
        -----
        The method used the `~/.local/share/geode-gem/<profile>/wine` directory
        to retrieve the game files.

        Parameters
        ----------
        core : geode_gem.core.Core
            The instance of the running main core used to retrieve the Geode-Gem
            local data directory to retrieve the WINE games
        """

        super().inspect(core)

        path = core.get_path_from_data("wine", self.identifier)
        for filename in path.glob("*.ini"):
            if filename.exists() and filename.is_file():
                game = WineGame.new_from_configuration(filename)
                game.collection = self
                self[game.identifier] = game
