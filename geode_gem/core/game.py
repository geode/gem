# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from dataclasses import dataclass
from datetime import datetime, timedelta
from hashlib import md5, sha256
from pathlib import Path
from typing import Any, NoReturn, Self, TYPE_CHECKING
import pickle

from xdg.DesktopEntry import DesktopEntry

from geode_gem.core.configuration import Configuration
from geode_gem.core.entity import Entity

if TYPE_CHECKING:
    from geode_gem.core.collection import GenericCollection
    from geode_gem.core.launcher import GenericLauncher


@dataclass
class GenericGame(Entity):
    """Basic class which represent a Game in the application

    Attributes
    ----------
    path : pathlib.Path
        The path to the file used to reference the game on the filesystem
    log_path : pathlib.Path
        The path to the log file used to store the output of the process when
        the game was launched
    note_path : pathlib.Path
        The path to the text file which store custom note about the game using
        the commonmark style
    played : int
        The number of time the game was launched
    checksum : str
        The unique hash to represent the game in the database
    last_played : datetime.datetime
        The date of the last time the game was launched
    last_played_time : datetime.timedelta
        The duration of the game session of the last launched time
    time_played : datetime.timedelta
        The total duration of all the game session
    favorite : bool
        Declare the game as favorite
    finished : bool
        Declare the game as finished
    multiplayer : bool
        Declare the game as multiplayer
    launcher : geode_gem.core.launcher.GenericLauncher
        The reference of the launcher associate with the game
    collection : geode_gem.core.collection.GenericCollection
        The reference of the collection associate with the game

    Notes
    -----
    The secondary table name constant ``DATABASE_TABLE`` is used to store
    information which are not generic.

    The dedicated table is define in the ``schemata`` attribute from the
    ``geode_gem.core.database.Database`` class.
    """

    path: Path = None
    log_path: Path = None
    note_path: Path = None

    played: int = 0
    checksum: str = ""

    last_played: datetime = None
    last_played_time: timedelta = None
    time_played: timedelta = None

    favorite: bool = False
    finished: bool = False
    multiplayer: bool = False

    launcher: "GenericLauncher" = None
    collection: "GenericCollection" = None

    # Secondary table name in the database
    DATABASE_TABLE = None

    def as_database_row(self: Self) -> dict[str, dict[str, Any]]:
        """Returns a structure readable by the database

        Returns
        -------
        dict
            The structure of the model to send in the database
        """

        return {
            "games": {
                "label": self.label,
                "icon": str(self.icon) if self.icon is not None else "",
                "favorite": int(self.favorite),
                "finished": int(self.finished),
                "multiplayer": int(self.multiplayer),
                "played": self.played,
                "last_played": (
                    self.last_played.isoformat(timespec="seconds")
                    if self.last_played is not None
                    else ""
                ),
                "last_played_time": (
                    str(self.last_played_time.total_seconds())
                    if self.last_played_time is not None
                    else "0.0"
                ),
                "time_played": (
                    str(self.time_played.total_seconds())
                    if self.time_played is not None
                    else "0.0"
                ),
                "environment": (
                    pickle.dumps(self.environment)
                    if len(self.environment) > 0
                    else None
                ),
            }
        }

    def calc_checksum(self: Self) -> NoReturn:
        """Generate a unique hash for the game based on it filepath"""

        checksum = md5()

        if self.path is not None:
            checksum.update(bytes(self.path.stem, "UTF-8"))
        else:
            checksum.update(bytes(self.identifier, "UTF-8"))

        self.checksum = checksum.hexdigest()

    def update(self: Self, data: dict[str, Any]) -> NoReturn:
        """Update the game instance attributes with the specified data

        Parameters
        ----------
        data : dict[str, Any]
            The structure used to update the game attributes
        """

        label = data.get("label")
        if len(label) > 0:
            self.label = label

        self.icon = data.get("icon")
        self.favorite = data.get("favorite")
        self.finished = data.get("finished")
        self.multiplayer = data.get("multiplayer")
        self.played = data.get("played")
        self.last_played = data.get("last_played")
        self.last_played_time = data.get("last_played_time")
        self.time_played = data.get("time_played")

        self.environment.clear()
        if (values := data.get("environment")) is not None:
            try:
                for key, value in pickle.loads(values):
                    self.putenv(key, value)
            except TypeError:
                pass


@dataclass
class ConsoleGame(GenericGame):
    """Advanced class to represent a Game for a Console collection (ROMs)

    Parameters
    ----------
    arguments : str
        The custom startup arguments used by the game during the launch process
    game_id : str
        The identifier related to the game, mostly used by the Dolphin emulator
    """

    arguments: str = ""
    game_id: str = ""

    DATABASE_TABLE = "roms"

    def as_database_row(self: Self) -> dict[str, dict[str, Any]]:
        data = super().as_database_row()
        data["roms"] = {
            "arguments": self.arguments,
            "emulator": self.launcher if self.launcher is not None else "",
            "game_id": self.game_id,
        }

        return data

    def calc_checksum(self: Self) -> NoReturn:
        data = sha256()

        # The collection identifier is used to ensure two files with the same
        # name but under different collection will not have the same checksum
        data.update(bytes(self.collection.identifier, "UTF-8"))

        # Add relative collection path to have the same checksum, even if the
        # user move the collection elsewhere. Also, this value help to have a
        # different checksum, even if two files with the same name are in the
        # same collection but in different directories
        path = self.path.relative_to(self.collection.path)
        data.update(str(path).encode(errors="ignore"))

        chunked_bytes = int()
        with self.path.open("rb") as fb:
            # The size of the chuck used to read the ROM file
            while chunk := fb.read(8192):
                data.update(chunk)

                chunked_bytes += 8192
                # The maximal bytes size to retrieve from the ROM file.
                # These data are used to calc the uniq checksum.
                if chunked_bytes > 100000:
                    break

        self.checksum = data.hexdigest()

    def update(self: Self, data: dict[str, Any]) -> NoReturn:
        super().update(data)
        self.arguments = data.get("arguments")
        self.game_id = data.get("game_id")


@dataclass
class NativeGame(GenericGame):
    """Advanced class to represent a Game on a GNU/Linux distribution

    This class is based on the FreeDesktop specifications about the DesktopEntry

    https://specifications.freedesktop.org/desktop-entry-spec/latest/

    Attributes
    ----------
    executable : str
        The content of the Exec option from the entry
    desktop_entry : xdg.DesktopEntry.DesktopEntry
        The instance of the parsed entry
    """

    executable: str = ""

    desktop_entry: DesktopEntry = None

    @classmethod
    def new_from_desktop_entry(cls: Self, entry: DesktopEntry) -> Self:
        """Generate a new instance of the NativeGame base on a DesktopEntry

        Parameters
        ----------
        entry : xdg.DesktopEntry.DesktopEntry
            The instance of the entry used to generate the NativeGame object

        Returns
        -------
        geode_gem.core.game.NativeGame
            The new instance of the NativeGame
        """

        path = Path(entry.filename).expanduser()

        return cls(
            path.stem,
            path=path,
            desktop_entry=entry,
            label=entry.getName(),
            icon=entry.getIcon(),
            executable=entry.getExec(),
        )


@dataclass
class ScummvmGame(GenericGame):
    """Advanced class to represent a Game using the SCUMMVM launcher"""

    def calc_checksum(self: Self) -> NoReturn:
        checksum = md5()
        checksum.update(bytes(self.identifier, "UTF-8"))
        self.checksum = checksum.hexdigest()

    @classmethod
    def new_from_data(
        cls: Self, section: str, **kwargs: dict[str, str]
    ) -> Self:
        return cls(
            kwargs.get("gameid", section),
            label=kwargs.get("description"),
        )


@dataclass
class WineGame(GenericGame):
    """Advanced class to represent a Game using the WINE layer

    Attributes
    ----------
    executable : pathlib.Path
        The path of the executable used to start the game
    wine_debug : bool
        Activate the debug mode in the wine session
    wine_prefix : pathlib.Path
        The path of the wine prefix directory (which contains drive_c, …)
    """

    executable: Path = None
    wine_prefix: Path = None
    wine_debug: bool = False

    def __post_init__(self: Self) -> NoReturn:
        """Update the prefix directory if this one is missing"""

        if self.wine_prefix is None:
            self.wine_prefix = Path("~/.wine")

    @classmethod
    def new_from_configuration(cls: Self, path: Path) -> Self:
        parser = Configuration(path)

        return cls(
            path.stem,
            path=path,
            label=parser.get("metadata", "label", fallback=path.stem),
            icon=parser.get("metadata", "icon", fallback=path.stem),
            executable=parser.get("metadata", "executable"),
            launcher=parser.get("metadata", "launcher", fallback=None),
            wine_prefix=parser.getpath("metadata", "prefix", fallback=None),
            wine_debug=parser.getboolean("options", "debug", fallback=False),
        )
