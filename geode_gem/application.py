# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from dataclasses import dataclass, field
from datetime import datetime, timedelta
from gettext import textdomain, bindtextdomain
from logging import Logger
from os import name as os_name
from pathlib import Path
from threading import enumerate as thread_enumerate
from typing import Any, NoReturn, Self, TYPE_CHECKING

from gi import require_version

require_version("Gtk", "3.0")
from gi.repository import GLib, GObject, Gtk

from xdg.BaseDirectory import xdg_cache_home

from geode_gem.core.lock_file import init_lock_file
from geode_gem.core.metadata import Metadata
from geode_gem.core.utils import get_data
from geode_gem.interface import GeodeGemInterface
from geode_gem.thread import GameThread
from geode_gem.configuration import ApplicationConfiguration

if TYPE_CHECKING:
    from geode_gem.core import Core
    from geode_gem.core.collection import GenericCollection
    from geode_gem.core.game import GenericGame
    from geode_gem.core.launcher import GenericLauncher


@dataclass
class Application(Gtk.Application):
    """Manage the Geode-Gem core and prepare the main application

    Notes
    -----
    This class is a bridge between the Core and the GeodeGemWindow

    Attributes
    ----------
    core : geode_gem.core.Core
        The instance of the Geode-Gem core
    logger : logging.Logger
        The instance of the logging module
    has_debug : bool
        Define the debug flag of the application based on the logging level
    lock_file : Path
        The path to the file used to mark the application as running
    cache_directory : Path
        The path to the cache directory used to store the cropped images
    config : geode_gem.configuration.ApplicationConfiguration
        The instance of the configuration dedicated to store the application
        options
    window : gi.repository.Gtk.ApplicationWindow
        The instance of the main window related to the application
    collections : dict[str, geode_gem.core.collections.GenericCollection]
        The storage for the collection used to cache in memory the game entries
        and avoid heavy reloading
    started_games : dict[str, geode_gem.core.game.GenericGame]
        The storage for the game currently running in the application
    """

    core: "Core"
    logger: Logger

    has_debug: bool = False

    lock_file: Path | None = None
    cache_directory: Path | None = None

    config: ApplicationConfiguration | None = None
    window: Gtk.ApplicationWindow | None = None

    collections: dict[str, "GenericCollection"] = field(default_factory=dict)
    started_games: dict[str, "GenericGame"] = field(default_factory=dict)

    shortcuts = {
        "win.quit": "quit",
    }

    __gsignals__ = {
        "crash-game": (GObject.SignalFlags.RUN_FIRST, None, (object, object)),
        "start-game": (GObject.SignalFlags.RUN_FIRST, None, (object,)),
        "terminate-game": (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (object, object, object),
        ),
    }

    def __post_init__(self: Self) -> NoReturn:
        """Initialize the Gtk.Application and the debug attribute"""

        super().__init__(application_id="org.geode.gem")

        self.has_debug = self.logger.getEffectiveLevel() == 10

    def __init_cache_directory__(self: Self) -> NoReturn:
        """Generate the cache directory for the current Geode-Gem profile"""

        self.cache_directory = Path(
            xdg_cache_home, Metadata.Python.APPLICATION, self.core.profile_name
        )

        self.logger.info(
            f"Initialize the cache directory '{self.cache_directory}'"
        )
        self.cache_directory.mkdir(parents=True, exist_ok=True)

    def __init_configuration__(self: Self) -> NoReturn:
        """Initialize the configuration for the current Geode-Gem profile"""

        path = self.core.get_path_from_config("config.ini")

        self.logger.info(
            f"Initialize the configuration manager with the file '{path}'"
        )
        self.config = ApplicationConfiguration(path, logger=self.logger)

    def __init_localization__(self: Self) -> NoReturn:
        """Initialize the localization of Geode-Gem"""

        egg_name = Metadata.Python.LOCALIZATION

        self.logger.info(
            f"Initialize the localization for the Python egg '{egg_name}'"
        )
        bindtextdomain(egg_name, localedir=str(get_data("data", "i18n")))
        textdomain(egg_name)

    def __init_lock_file__(self: Self) -> NoReturn:
        """Initialize the lock file of Geode-Gem

        Notes
        -----
        This lock-file is only available on POSIX systems
        """

        # The lock file manage is only support on POSIX system
        if not os_name == "posix":
            self.logger.warning(
                "This is not a POSIX system, the lock file manager will be "
                "ignored"
            )
            return

        self.lock_file = self.core.get_path_from_data(".lock")

        self.logger.info(f"Initialize the lock file '{self.lock_file}'")
        init_lock_file(self.lock_file)

    def __register_accelerator__(
        self: Self, action_name: str, key: str
    ) -> NoReturn:
        """Register an accelerator for a detailed action name

        Parameters
        ----------
        action_name : str
            The detailed action name which will be used when the accelerator is
            activate
        key : str
            The name of the option in the keys section from the configuration
            file

        Raises
        ------
        ValueError
            When the specified accelerator do not exists anywhere
            When the specified accelerator cannot be parse correctly by Gtk
        """

        accelerator = self.config.get_accelerator(key)
        if accelerator is None:
            raise ValueError(
                f"No value for the key {key} available in the configuration"
            )

        key, mods = Gtk.accelerator_parse(accelerator)
        if key == 0 or key is None:
            raise ValueError(
                f"Cannot add the accelerator '{accelerator}' to {action_name}"
            )

        self.logger.debug(
            f"Add the accelerator '{accelerator}' for {action_name}"
        )
        self.set_accels_for_action(action_name, [accelerator])

    def alternate_game_state(self: Self, game: "GenericGame") -> NoReturn:
        """Alternate the specified game state between running and terminate

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game to alternate
        """

        if game is not None:
            if self.check_game_is_started(game):
                thread = self.get_thread_for_game(game)
                thread.processus.terminate()
                thread.join()
            else:
                self.do_start_game(game)

    def check_game_is_started(self: Self, game: "GenericGame") -> bool:
        """Check if the specified game is currently running

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game used for the verification

        Returns
        -------
        bool
            True if the game is running, False otherwise
        """

        return game.identifier in self.started_games.get(
            game.collection.identifier, {}
        )

    def do_activate(self: Self) -> NoReturn:
        """Generate the main window with this current application"""

        if self.window is None:
            self.window = GeodeGemInterface(application=self)
            self.window.run()
            self.window.set_focus(None)

        self.window.present()

    def do_crash_game(
        self: Self, game: "GenericGame", exception: Exception
    ) -> NoReturn:
        """Receive a crash exception from the game launching process

        Parameters
        ----------
        game : geode_gem.core.game.Game
            The instance of the game used to retrieve the information
        exception : Exception
            The exception raised during the crash
        """

        self.window.show_game_error_message(game, exception)

    def do_shutdown(self: Self) -> NoReturn:
        """Shutdown the application propertly and stop remaining processus"""

        Gtk.Application.do_shutdown(self)

        self.logger.info("Prepare to close the main application")

        # Remove any remaining thread
        for thread in thread_enumerate().copy():
            if isinstance(thread, GameThread):
                self.logger.debug(f"Close the game thread [{thread.name}]")
                thread.processus.terminate()
                thread.join()

                # Run the terminate game method manually since the emited signal
                # for the game termination process is received but not executed…
                self.do_terminate_game(
                    thread.game, thread.started_date, thread.terminated_date
                )

        # Save the configuration manager modification
        self.config.save()

        # Remove the lock file instance on filesystem
        if self.lock_file is not None and self.lock_file.exists():
            self.logger.info("Remove the lock file")
            self.lock_file.unlink()

        self.logger.info(f"Quit the {Metadata.NAME} application")

    def do_startup(self: Self) -> NoReturn:
        """Start the application and prepare the main interface

        Notes
        -----
        This method cannot be executed without a working DISPLAY

        Returns
        -------
        int
            The value of the errno code based on process status
        """

        Gtk.Application.do_startup(self)

        self.__init_localization__()
        self.__init_lock_file__()
        self.__init_cache_directory__()
        self.__init_configuration__()

        self.set_accelerators_from_user_config()

    def do_start_game(self: Self, game: "GenericGame") -> NoReturn:
        """Start a game in a dedicated thread

        Parameters
        ----------
        game : geode_gem.core.game.Game
            The instance of the game to run
        """

        self.logger.debug(f"Logging the process in the file {game.log_path}")

        collection_identifier = game.collection.identifier
        if collection_identifier not in self.started_games:
            self.started_games[collection_identifier] = dict()

        launcher = self.get_launcher_from_game(game)

        self.logger.info(f'Launch the game "{game.label}"')
        thread = GameThread(launcher, game, self.logger, self.emit)
        thread.start()

        self.started_games[collection_identifier][game.identifier] = thread

        self.window.update_game(game)

    def do_terminate_game(
        self: Self,
        game: "GenericGame",
        started_date: datetime,
        terminated_date: datetime,
    ) -> NoReturn:
        """Terminate a specific game and update the information of this game

        Parameters
        ----------
        game : geode_gem.core.game.Game
            The instance of the game to terminate
        started_date : datetime.datetime
            The date when the game have been launched
        terminated_date : datetime.datetime
            The date when the game have been closed
        """

        del self.started_games[game.collection.identifier][game.identifier]

        delta = terminated_date - started_date
        if delta.seconds == 0:
            self.logger.debug(
                f"The game {game.identifier} have a delta less than one "
                f"second: {delta}"
            )

        else:
            if game.time_played is None:
                game.time_played = timedelta()

            self.logger.info(f'Register the game metadata of "{game.label}"')
            game.last_played = started_date
            game.last_played_time = delta
            game.time_played += delta
            game.played += 1

            self.core.save_game(game)

        self.window.update_game(game)

    def emit(self: Self, *args, **kwargs) -> NoReturn:
        """Override the emit method of the Gtk.Application class

        Notes
        -----
        By overwritting this method, the method related to the main interface
        or the database instance are correctly used in the main thread instead
        of another thread (like the GameThread).

        See Also
        --------
        gi.repository.GLib.idle_add
        gi.repository.GObject.GObject.emit
        """

        GLib.idle_add(GObject.GObject.emit, self, *args, **kwargs)

    def get_collections(self) -> list["GenericCollection"]:
        """Retrieve the available collections sorted by label

        Returns
        -------
        list
            The list of collections sorted by label
        """

        return sorted(self.core.collections, key=lambda k: k.label.lower())

    def get_games_for_collection(self, collection: "GenericCollection"):
        """Retrieve the available games for a specific collection

        Parameters
        ----------
        collection : geode_gem.core.collection.GenericCollection
            The instance of the collection used to retrieve the list of games

        Yields
        ------
        int
            First loop, the progression of the initialization loop which read
            the database to retrieve information for each game
        geode_gem.core.game.GenericGame
            Second loop, the list of games sorted by the label
        """

        if collection.identifier not in self.collections:
            self.collections[collection.identifier] = []

        games = self.collections[collection.identifier]
        games.clear()

        for index, data in enumerate(collection):
            if data is not None:
                game = self.core.update_game_from_database(data)
                games.append(game)

            yield index

        for game in sorted(games, key=lambda k: k.label.lower()):
            yield game

    def get_launcher_from_game(
        self: Self, game: "GenericGame"
    ) -> "GenericLauncher":
        """Retrieve the launcher for the specified game

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game used to retrieve the launcher

        Returns
        -------
        geode_gem.core.launcher.GenericLauncher
            The instance of the launcher related to the game when a specific
            launcher was defined, otherwise the launcher related to the
            collection is retrieved
        """

        if game.launcher is not None:
            return game.launcher

        return game.collection.launcher

    def get_thread_for_game(
        self: Self, game: "GenericGame"
    ) -> GameThread | None:
        """Retrieve the stored thread for the game

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game used for the verification

        Returns
        -------
        geode_gem.thread.GameThread or None
            The instance of the thread related to the game if this one is
            currently running, None otherwise
        """

        return self.started_games.get(game.collection.identifier, {}).get(
            game.identifier, None
        )

    def get_window_size(
        self: Self, option: str, width: int = 800, height: int = 600
    ) -> tuple[int, int]:
        """Retrieve the dialog window geometry

        Parameters
        ----------
        option : str
            The name of the option related to the dialog in the configuration
        width : int
            The vertical size of the window
        height : int
            The horizontal size of the window

        Returns
        -------
        tuple[int, int]
            The tuple contains the geometry of the wanted dialog
        """

        return self.config.get_window_size(option, width=width, height=height)

    def on_quit(self: Self, *args, **kwargs) -> NoReturn:
        """Quit the main application"""

        self.quit()

    def save_game(self, game: "GenericGame", data: dict[str, Any]) -> NoReturn:
        """Save the new game information in the database

        Notes
        -----
        The game object provided as parameter will be altered in this function

        Parameters
        ----------
        game : geode_gem.core.game.GenericGame
            The instance of the game to update in the database and itself
        data : dict[str, typing.Any]
            The information from the game as a dictionary structure
        """

        for key, value in data.get("general").items():
            if hasattr(game, key):
                setattr(game, key, value)

        self.logger.debug(f"Save modification for '{game}' in the database")
        self.core.save_game(game)

    def set_accelerators_from_user_config(self: Self) -> NoReturn:
        """Define the interface accelerators based from the configuration"""

        self.logger.info("Initialize the accelerators for the main interface")
        for key, value in self.shortcuts.items():
            try:
                self.__register_accelerator__(key, value)
            except ValueError as error:
                self.logger.warning(error)

    def set_window_size(
        self: Self, option: str, width: int, height: int
    ) -> NoReturn:
        """Define the specified window geometry for a specific dialog

        Parameters
        ----------
        option : str
            The name of the option related to the dialog in the configuration
        width : int
            The vertical size of the window
        height : int
            The horizontal size of the window
        """

        self.config.update_option("window", option, f"{width} {height}")
