# Copyleft 2015-2025 PacMiam
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from dataclasses import dataclass, KW_ONLY
from logging import Logger
from typing import Any, NoReturn, Self

from gi import require_version

require_version("Gtk", "3.0")

from gi.repository import GObject, Gtk

from geode_gem.core.configuration import Configuration


class GamesColumns(GObject.GEnum):
    """Represents the columns for sorting the games in the filtering menu"""

    FAVORITE = 0
    LABEL = 1
    FINISHED = 2
    MULTIPLAYER = 3


@dataclass
class ApplicationConfiguration(Configuration):
    """The Geode-Gem configuration manager

    Attributes
    ----------
    logger : logging.Logger
        The instance of the logger
    """

    _ = KW_ONLY
    logger: Logger | None = None

    # Store the default accelerators for the application
    default_accelerators = {
        "quit": "<Control>q",
    }

    @property
    def games_sorted_column(self: Self) -> GamesColumns:
        """Retrieve the games list sorted column

        Returns
        -------
        geode_gem.interface.utils.GamesColumns
            The flag related to the configuration value
        """

        value = self.get("games", "sorted-by", fallback="label")

        match value.strip().lower():
            case "favorite":
                return GamesColumns.FAVORITE
            case "finished":
                return GamesColumns.FINISHED
            case "multiplayer":
                return GamesColumns.MULTIPLAYER
            case _:
                return GamesColumns.LABEL

    @property
    def games_sorted_order(self: Self) -> Gtk.SortType:
        """Retrieve the order of the games list sorted column

        Returns
        -------
        gi.repository.Gtk.SortType
            The flag related to the configuration value
        """

        value = self.get("games", "sorted-order", fallback="descending")

        match value.strip().lower():
            case "ascending":
                return Gtk.SortType.ASCENDING
            case _:
                return Gtk.SortType.DESCENDING

    def get_accelerator(self: Self, option: str) -> str | None:
        """Retrieve the accelerator associated with the specified option

        Parameters
        ----------
        option : str
            The name of the accelerator from the keys section in the
            configuration

        Returns
        -------
        str | None
            The shortcut related to the specified option if defined in the
            keys section from the configuration file, otherwise returns the
            default accelerator
        """

        return self.get(
            "keys", option, fallback=self.default_accelerators.get(option, None)
        )

    def get_window_size(
        self: Self, option: str, width: int = 800, height: int = 600
    ) -> tuple[int, int]:
        """Retrieve the window size for a specific dialog

        Parameters
        ----------
        option : str
            The name of the option related to the dialog in the window section
        width : int
            The default width to fallback if the option is missing or invalid
        height : int
            The default height to fallback if the option is missing or invalid

        Returns
        -------
        tuple(int, int)
            Width and height of this dialog
        """

        value = self.get("window", option, fallback=f"{width} {height}").strip()

        try:
            config_width, config_height = value.split()
            return (int(config_width), int(config_height))

        except ValueError:
            self.logger.warning(
                "Cannot parse the following value for game properties dialog "
                f"size: {value}"
            )
            return (width, height)

    def update_option(
        self: Self, section: str, option: str, value: Any
    ) -> NoReturn:
        """Update a specific option from the configuration manager

        Parameters
        ----------
        section : str
            The name of the section which contains the option
        option : str
            The name of the option to update
        value : str or int or bool
            The new value for the specified options
        """

        # Set a valid string for boolean values
        if isinstance(value, bool):
            value = "yes" if value else "no"

        if not self.has_section(section):
            self.add_section(section)

        self.set(section, option, str(value))
